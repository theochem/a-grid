! Written by Niek de Klerk, 2014, at the Radboud University Nijmegen. 

! q-GRID and Bader are free software: free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! A copy of the GNU General Public License is available at
! http://www.gnu.org/licenses/

!---------------------------------------------------------
! Contains the functions to calculate the interactions
!---------------------------------------------------------
MODULE interactions_mod
! Contains the functions needed to calcalute interactions for molecules_mod.f90
! Everywhere atomic units are used for distances and charges

        USE kind_mod
        USE ions_mod
        USE charge_mod
        USE functions_mol_mod
        USE options_mod
        USE mpi

        CONTAINS

!!!!!!!!!!!!!!!!!!!!!!!!!
SUBROUTINE grid_grid_mpi(chg_new,coulombE,cellx,celly,cellz,nr_of_mol,nr_inter_mol,myrank,nproc,unitcells,tot_points_now)
!Calculates Coulomb-interaction between gridpoints, using parallel processors
! 1) Each processors does certain numbers (na), and calculates the interactions of this point with all the other points. 
! 2) It saves all the interactions, and when it's totally done with calculations it sends them to core 0
! 3) The values of all cores are added to the total, and written to several files.

      TYPE(chg_row),INTENT(IN) :: chg_new
      TYPE(coulomb_E), INTENT(INOUT) :: coulombE
      INTEGER, INTENT(IN) :: cellx,celly,cellz,nr_of_mol,nr_inter_mol,myrank,nproc,unitcells,tot_points_now

      INTEGER :: na,nb,mola,molb,nr_mol2,nr_cellz,nr_celly,nr_cellx,i,inter_nr,inter_nr_sym
      INTEGER :: ucx,ucy,ucz,xper,yper,zper,xper_sym,yper_sym,zper_sym,uc_nr,uc_nr_sym
      REAL(q2) :: chga,chgb,interaction,dist
      REAL(q2), DIMENSION(3) :: place_a,place_b,place_2
      LOGICAL, DIMENSION(:),ALLOCATABLE :: not_done
      INTEGER :: ierror,istatus

      ALLOCATE(not_done(unitcells))
      not_done(:) = .TRUE.
      nr_mol2 = nr_of_mol*nr_of_mol
      nr_cellz = 2*cellz + 1
      nr_celly = 2*celly + 1
      nr_cellx = 2*cellx + 1

      IF (myrank .EQ. 0) THEN       
         write(*,'(A,$)') 'Calculating grid-grid interactions: '
      ENDIF
     
      DO na = tot_points_now,1,-1 
         IF (mod(na,nproc) .EQ. myrank) THEN !Determines which points each processor does
            mola = chg_new%mol(na)         !molecule of point A
            place_a = chg_new%pos_car(na,:)   !A in 0,0,0
            chga =  chg_new%e_charge(na)    !charge of point A
            DO nb = na,1,-1
               molb = chg_new%mol(nb)        !molecule of point B
               place_b = chg_new%pos_car(nb,:) !B in 0,0,0
               chgb = chg_new%e_charge(nb)   !charge of point B
               interaction = chga * chgb !*epsilon-term (=1) 
               DO ucx = cellx,-cellx,-1 !Loop over all unit cells
                  xper = ucx + cellx + 1
                  xper_sym = (-ucx) + cellx + 1 !the symmetry equivalent unit cell
                  DO ucy = celly,-celly,-1
                     yper = ucy + celly + 1  
                     yper_sym = (-ucy) + celly + 1 
                     DO ucz = cellz,-cellz,-1
                        zper = ucz + cellz + 1
                        uc_nr = zper + (yper-1)*nr_cellz + (xper-1)*nr_cellz*nr_celly
                        IF (not_done(uc_nr)) THEN !if the unit cell is not done already
                           zper_sym = (-ucz) + cellz + 1                             
                           uc_nr_sym = zper_sym + (yper_sym-1)*nr_cellz + (xper_sym-1)*nr_cellz*nr_celly
                           IF ( (ucz .NE. 0) .OR. (ucy .NE. 0) .OR. (ucx .NE. 0) ) THEN !gridpoint in other unit cell
                              !from A in 0,0,0 to B in ucx,ucy,ucz AND B in 0,0,0 to A in -ucx,-ucy,-ucz
                              inter_nr = ((uc_nr-1) * nr_mol2) + ((mola-1) * nr_of_mol) + molb !a to b
                              inter_nr_sym = ((uc_nr_sym-1) * nr_mol2) + ((molb-1) * nr_of_mol) + mola !b to a
                              place_2(1) = place_b(1) + coulombE%distx(inter_nr) !x-coordinate 
                              place_2(2) = place_b(2) + coulombE%disty(inter_nr) !y
                              place_2(3) = place_b(3) + coulombE%distz(inter_nr) !z
                              dist = sqrt( (place_a(1)-place_2(1))**2 + (place_a(2)-place_2(2))**2 + (place_a(3)-place_2(3))**2)   
                              coulombE%grid_grid(inter_nr) = coulombE%grid_grid(inter_nr) + (interaction/dist)*0.5 
                              coulombE%grid_grid(inter_nr_sym) = coulombE%grid_grid(inter_nr_sym) + (interaction/dist)*0.5 
                              IF (na .NE. nb) THEN
                                !from B in 0,0,0 to A in ucx,ucy,ucz AND A in 0,0,0 to B in -ucx,-ucy,-ucz
                                 inter_nr = ((uc_nr-1) * nr_mol2) + ((molb-1) * nr_of_mol) + mola !b to a 
                                 inter_nr_sym = ((uc_nr_sym-1) * nr_mol2) + ((mola-1) * nr_of_mol) + molb !a to b
                                 place_2(1) = place_a(1) + coulombE%distx(inter_nr) !x-coordinaat 
                                 place_2(2) = place_a(2) + coulombE%disty(inter_nr) !y
                                 place_2(3) = place_a(3) + coulombE%distz(inter_nr) !z
                                 dist = sqrt((place_b(1)-place_2(1))**2 +(place_b(2)-place_2(2))**2 +(place_b(3)-place_2(3))**2)   
                                 coulombE%grid_grid(inter_nr) = coulombE%grid_grid(inter_nr) + (interaction/dist)*0.5  
                                 coulombE%grid_grid(inter_nr_sym) = coulombE%grid_grid(inter_nr_sym) + (interaction/dist)*0.5
                              ENDIF
                           ELSEIF (mola .NE. molb) THEN !in unit cell 0,0,0 but other molecules
                              inter_nr = ((uc_nr-1) * nr_mol2) + ((mola-1) * nr_of_mol) + molb !a to b
                              inter_nr_sym = ((uc_nr_sym-1) * nr_mol2) + ((molb-1) * nr_of_mol) + mola !b to a
                              dist = sqrt((place_a(1)-place_b(1))**2 + (place_a(2)-place_b(2))**2 + (place_a(3)-place_b(3))**2)
                              coulombE%grid_grid(inter_nr) = coulombE%grid_grid(inter_nr) + (interaction/dist)*0.5 
                              coulombE%grid_grid(inter_nr_sym) = coulombE%grid_grid(inter_nr_sym) + (interaction/dist)*0.5
                           ENDIF
                           not_done(uc_nr) = .FALSE. !unit cell is done
                           not_done(uc_nr_sym) = .FALSE.
                        ENDIF !not_done
                     ENDDO !ucz
                  ENDDO !ucy
               ENDDO !ucx
               not_done(:) = .TRUE. !reset all the unit cells to not_done
            ENDDO !nb1
            IF ( mod(na, tot_points_now/20) .EQ. 0 )  THEN !5% of the calculations done              
               write(*,'(A,$)') '*'
            ENDIF
         ENDIF !check mod( ,nproc).EQ. myrank
      ENDDO !na1

! Send the calculated values to proc 0, and write the result to a file
      IF (myrank .EQ. 0) THEN
         ALLOCATE(coulombE%energy_recv(nr_inter_mol))
         coulombE%energy_recv(:) = 0
         write(*,*) ''
         write(*,*) 'processor 0 receiving....'
         DO i = 1,nproc-1
            CALL MPI_RECV(coulombE%energy_recv(:),nr_inter_mol,MPI_REAL8,i,5000+i,MPI_COMM_WORLD,istatus,ierror) !receive coulombE from each processor 
            coulombE%grid_grid(:) = coulombE%grid_grid(:) + coulombE%energy_recv(:)  !add coulombE
         ENDDO
         write(*,*) 'Grid-grid interaction:', sum(coulombE%grid_grid(:)), 'hartree'
         DEALLOCATE(coulombE%energy_recv)
      ELSE
         CALL MPI_SEND(coulombE%grid_grid(:),nr_inter_mol,MPI_REAL8,0,5000+myrank,MPI_COMM_WORLD,istatus,ierror) ! Send coulombE
      ENDIF 
      DEALLOCATE(not_done)

END SUBROUTINE grid_grid_mpi

!-------------------------------------------------------------------------------------------------------------

SUBROUTINE core_grid_mpi(chg,coulombE,cellx,celly,cellz,nr_of_mol,nr_inter_mol,myrank,nproc,npts,unitcells,nr_of_atoms, & 
        & molecule,new_car,charge)
!Calculate the Coulomb-interactions between a core and a gridpoint, using parallel processors
! each processor is assigned a certain atom and calculates all the interactions of this atom with all gridpoints.
      TYPE(charge_obj),INTENT(IN) :: chg 
      TYPE(coulomb_E), INTENT(INOUT) :: coulombE
      INTEGER, INTENT(IN) :: cellx,celly,cellz,nr_of_mol,nr_inter_mol,myrank,nproc,unitcells,nr_of_atoms
      INTEGER,DIMENSION(3),INTENT(IN) :: npts
      INTEGER,DIMENSION(:),INTENT(IN) :: molecule
      REAL(q2),DIMENSION(:,:),INTENT(IN) :: new_car
      REAL(q2),DIMENSION(:),INTENT(IN) :: charge

      INTEGER :: na,mola,nb1,nb2,nb3,molb,ucx,ucy,ucz,uc_nr,uc_nr_sym,inter_nr,inter_nr_sym,atom_b
      INTEGER :: xper,xper_sym,yper,yper_sym,zper,zper_sym,i,nr_mol2,nr_cellz,nr_cellx,nr_celly
      REAL(q2) :: interaction,chga,chgb,dist
      REAL(q2),DIMENSION(3) :: place_core,place_grid,place_c,place_g
      LOGICAL, DIMENSION(:),ALLOCATABLE :: not_done
      INTEGER :: istatus,ierror

      ALLOCATE(not_done(unitcells))
      not_done(:) = .TRUE.
      nr_mol2 = nr_of_mol*nr_of_mol
      nr_cellz = 2*cellz + 1
      nr_celly = 2*celly + 1
      nr_cellx = 2*cellx + 1

      IF (myrank .EQ. 0) THEN
         write(*,*) 'Calculating core-grid interactions'
         write(*,'(2X,A,$)') 'Cores done: '
      ENDIF

      DO na = 1,nr_of_atoms
         IF (mod(na,nproc) .EQ. myrank) THEN
            chga = charge(na)!ions%charge(na)       
            place_core = new_car(na,:) !ions%new_car(na,:)
            mola = molecule(na) !ions%molecule(na) !molecule of core A
            DO nb3 = 1,npts(3) 
               DO nb2 = 1,npts(2)
                  DO nb1 = 1,npts(1)
                     atom_b = chg%atom(nb1,nb2,nb3)
                     molb = chg%molecule(nb1,nb2,nb3) 
                     place_grid = chg%pos_car(nb1,nb2,nb3,:)     
                     chgb = chg%e_charge(nb1,nb2,nb3)  
                     !IF (chgb .GT. 0) THEN
                        interaction = chga * (-chgb)
            DO ucx = -cellx,cellx !loop over all unit cells
               xper = ucx + cellx + 1 
               xper_sym = (-ucx) + cellx + 1
               DO ucy = -celly,celly
                  yper = ucy + celly + 1  
                  yper_sym = (-ucy) + celly + 1 
                  DO ucz = -cellz,cellz
                     zper = ucz + cellz + 1
                     uc_nr = zper + (yper-1)*nr_cellz + (xper-1)*nr_cellz*nr_celly
                     IF (not_done(uc_nr)) THEN
                        zper_sym = (-ucz) + cellz + 1
                        uc_nr_sym = zper_sym + (yper_sym-1)*nr_cellz + (xper_sym-1)*nr_cellz*nr_celly
                        IF ((ucz .NE. 0) .OR. (ucy .NE. 0) .OR. (ucx .NE. 0)) THEN !in other unit cell
                           !for core in 0,0,0 to gridpoint in other unit cell
                           inter_nr = ((uc_nr-1) * nr_mol2) + ((mola-1) * nr_of_mol) + molb
                           inter_nr_sym = ((uc_nr_sym-1) * nr_mol2) + ((molb-1) * nr_of_mol) + mola
                           place_g(1) = place_grid(1) + coulombE%distx(inter_nr) !x-coordinate gridpoint
                           place_g(2) = place_grid(2) + coulombE%disty(inter_nr) !y             
                           place_g(3) = place_grid(3) + coulombE%distz(inter_nr) !z
                           dist = sqrt((place_core(1)-place_g(1))**2 +(place_core(2)-place_g(2))**2 +(place_core(3)-place_g(3))**2)
                           coulombE%core_grid(inter_nr) =  coulombE%core_grid(inter_nr) + 0.5*(interaction/dist)
                           coulombE%core_grid(inter_nr_sym) =  coulombE%core_grid(inter_nr_sym) + 0.5*(interaction/dist)
                           !for gridpoint in 0,0,0 to core in other unit cell
                           inter_nr = ((uc_nr-1) * nr_mol2) + ((molb-1) * nr_of_mol) + mola
                           inter_nr_sym = ((uc_nr_sym-1) * nr_mol2) + ((mola-1) * nr_of_mol) + molb
                           place_c(1) = place_core(1) + coulombE%distx(inter_nr) !x-coordinate core
                           place_c(2) = place_core(2) + coulombE%disty(inter_nr) !y             
                           place_c(3) = place_core(3) + coulombE%distz(inter_nr) !z
                           dist = sqrt((place_grid(1)-place_c(1))**2 +(place_grid(2)-place_c(2))**2 +(place_grid(3)-place_c(3))**2)
                           coulombE%core_grid(inter_nr) =  coulombE%core_grid(inter_nr) + 0.5*(interaction/dist)
                           coulombE%core_grid(inter_nr_sym) =  coulombE%core_grid(inter_nr_sym) + 0.5*(interaction/dist)
                        ELSEIF (mola .NE. molb) THEN !in unit cell 0,0,0 but other molecules
                           inter_nr = ((uc_nr-1) * nr_mol2) + ((mola-1) * nr_of_mol) + molb
                           inter_nr_sym = ((uc_nr-1) * nr_mol2) + ((molb-1) * nr_of_mol) + mola
                           dist = sqrt((place_core(1)-place_grid(1))**2+ (place_core(2)-place_grid(2))**2 & 
                                      & + (place_core(3)-place_grid(3))**2)
                           !core to gridpoint in 0,0,0 
                           coulombE%core_grid(inter_nr) =  coulombE%core_grid(inter_nr) + 0.5*(interaction/dist)
                           !gridpoint to core in 0,0,0
                           coulombE%core_grid(inter_nr_sym) =  coulombE%core_grid(inter_nr_sym) + 0.5*(interaction/dist)
                        ENDIF
                        not_done(uc_nr) = .FALSE.
                        not_done(uc_nr_sym) = .FALSE.
                     ENDIF
                  ENDDO !ucz
               ENDDO !ucy
            ENDDO !ucx
            not_done(:) = .TRUE. !reset unit cells to start with the next interaction
                     !ENDIF !chgb .GT. 0
                  ENDDO !nb1
               ENDDO
            ENDDO !nb3
            write(*,'(A,$)') '*' !shows that all the interactions with 1 atom are calculated
         ENDIF !mod()
      ENDDO !na

      IF (myrank .EQ. 0) THEN
         ALLOCATE(coulombE%energy_recv(nr_inter_mol))
         coulombE%energy_recv(:) = 0
         DO i = 1,nproc-1
            CALL MPI_RECV(coulombE%energy_recv(:),nr_inter_mol,MPI_REAL8,i,4000+i,MPI_COMM_WORLD,istatus,ierror) !receive coulomb E 
            coulombE%core_grid(:) = coulombE%core_grid(:) + coulombE%energy_recv(:)  !adding coulombE
         ENDDO
         DEALLOCATE(coulombE%energy_recv)
      ELSE
         CALL MPI_SEND(coulombE%core_grid(:),nr_inter_mol,MPI_REAL8,0,4000+myrank,MPI_COMM_WORLD,istatus,ierror) !sending coulombE
      ENDIF

      DEALLOCATE(not_done)

      IF (myrank .EQ. 0) THEN
         write(*,*) ''
         write(*,*) 'Total core-grid interaction', sum(coulombE%core_grid(:)), 'hartree'
      ENDIF
      
END SUBROUTINE core_grid_mpi

!--------------------------------------------------------------------------------------------------

SUBROUTINE core_core(ions,coulombE,cellx,celly,cellz,nr_of_mol,nr_of_atoms,opts)
!Calculates the Coulomb-, C6- and C12-interactions between cores, this all done on 1 processor.
      TYPE(ions_obj),INTENT(IN) :: ions
      TYPE(coulomb_E), INTENT(INOUT) :: coulombE
      TYPE(options_obj), INTENT(IN) :: opts
      INTEGER,INTENT(IN) :: cellx,celly,cellz,nr_of_mol,nr_of_atoms

      INTEGER :: na,nb,mola,molb,nr_cellx,nr_celly,nr_cellz
      INTEGER :: ucx,ucy,ucz,xper,yper,zper,uc_nr,inter_nr,nr_mol2,inter_nr_sym
      REAL(q2) :: chga,chgb,vdw_dist,C6,C12,dist,vdwE,repE,interaction, min_dist
      REAL(q2), DIMENSION(3) :: place_1,place_2
        
      nr_mol2 = nr_of_mol*nr_of_mol
      nr_cellz = 2*cellz + 1
      nr_celly = 2*celly + 1
      nr_cellx = 2*cellx + 1

		min_dist = 2.83 !in atomic units (bohr), Angstrom = atomic units*0.52918, 2.83 atomic units = 1.5 Angstrom

      DO na = 1,nr_of_atoms
         chga = ions%charge(na)
         mola = ions%molecule(na) !molecule of core A
         DO nb = na,nr_of_atoms !1,ions%nions
            chgb = ions%charge(nb)
            molb = ions%molecule(nb)   !molecule of core B
            interaction = chga * chgb
            vdw_dist = ions%vdw_dist(na) + ions%vdw_dist(nb)
            C6 = sqrt(ions%c6(na) * ions%c6(nb)) !Determine C6-coefficients between an atom-pair, method same as in CRYSTAL09
            C12 = 0.5*C6*(vdw_dist**6) !C12-coefficient for an atom-pair
            DO ucx = -cellx,cellx !loop over all unit cells
               xper = ucx + cellx + 1
               DO ucy = -celly,celly
                  yper = ucy + celly + 1
                  DO ucz = -cellz, cellz
                     zper = ucz + cellz + 1
                     uc_nr = zper + (yper-1)*nr_cellz + (xper-1)*nr_cellz*nr_celly
                     IF ((ucz .NE. 0) .OR. (ucy .NE. 0) .OR. (ucx .NE. 0)) THEN !in other unit cell
                        !mol A in 0,0,0 to B in ucx,ucy,ucz
                        inter_nr = ((uc_nr-1) * nr_mol2) + ((mola-1) * nr_of_mol) + molb
                        place_1(:) = ions%new_car(na,:) 
                        place_2(1) = ions%new_car(nb,1) + coulombE%distx(inter_nr) !x-coordinate 
                        place_2(2) = ions%new_car(nb,2) + coulombE%disty(inter_nr) !y
                        place_2(3) = ions%new_car(nb,3) + coulombE%distz(inter_nr) !z
                        dist = sqrt( (place_1(1)-place_2(1))**2 + (place_1(2)-place_2(2))**2 + (place_1(3)-place_2(3))**2 ) !sqrt((place_1 - place_2)^2)                       
								IF (dist .LE. min_dist) THEN
WRITE(*,*) '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
WRITE(*,*) '!! WARNING  Very small interatomic distance between 2 molecules detected (distance <', min_dist*0.52918, 'Angstrom) !'
WRITE(*,*) '!! WARNING  The C6 and C12 approximation for the van der Waals energy fails at such small distances !!'
WRITE(*,*) '!! WARNING  The interaction is between molecule', mola,' and molecule', molb, 'in unitcell ', ucx, ucy, ucz
WRITE(*,*) '!! WARNING  DO NOT USE the calculated value for this intermolecular interaction and its symmetric equivalents !!'
WRITE(*,*) '!! WARNING  The program does not ignore this value automatically, substract it from the total energies yourself !!'
WRITE(*,*) '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
								ENDIF
                        vdwE = -(C6/(dist**6)) 
                        repE = C12/(dist**12) 
                       ! add the interactions:
                        coulombE%c6(inter_nr) = coulombE%c6(inter_nr) + 0.5*vdwE
                        coulombE%c12(inter_nr) = coulombE%c12(inter_nr) + 0.5*repE
                        coulombE%core_core(inter_nr) = coulombE%core_core(inter_nr) + 0.5*(interaction/dist)
                        IF (na .NE. nb) THEN !mol B in 0,0,0 to A in ucx,ucy,ucz
                           inter_nr = ((uc_nr-1) * nr_mol2) + ((molb-1) * nr_of_mol) + mola
                           place_1(:) = ions%new_car(nb,:) 
                           place_2(1) = ions%new_car(na,1) + coulombE%distx(inter_nr) !x-coordinate 
                           place_2(2) = ions%new_car(na,2) + coulombE%disty(inter_nr) !y
                           place_2(3) = ions%new_car(na,3) + coulombE%distz(inter_nr) !z
                           dist = sqrt( (place_1(1)-place_2(1))**2 + (place_1(2)-place_2(2))**2 + (place_1(3)-place_2(3))**2 ) !SQRT((place_1 - place_2)^2)                       
									IF (dist .LE. min_dist) THEN
WRITE(*,*) '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
WRITE(*,*) '!! WARNING  Very small interatomic distance between 2 molecules detected (distance <', min_dist*0.52918, 'Angstrom) !!'
WRITE(*,*) '!! WARNING  The C6 and C12 approximation for the van der Waals energy fails at such small distances !!'
WRITE(*,*) '!! WARNING  The interaction is between molecule', mola,' and molecule', molb, 'in unitcell ', ucx, ucy, ucz
WRITE(*,*) '!! WARNING  DO NOT USE the calculated value for this intermolecular interaction and its symmetric equivalents !!'
WRITE(*,*) '!! WARNING  The program does not ignore this value automatically, substract it from the total energies yourself !!'
WRITE(*,*) '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
									ENDIF
                           vdwE = -(C6/(dist**6))
                           repE = C12/(dist**12)
	                     ! add the interactions:
                           coulombE%c6(inter_nr) = coulombE%c6(inter_nr) + 0.5*vdwE
                           coulombE%c12(inter_nr) = coulombE%c12(inter_nr) + 0.5*repE
                           coulombE%core_core(inter_nr) = coulombE%core_core(inter_nr) + 0.5*(interaction/dist)
                        ENDIF !na .NE. nb
                     ELSEIF (mola .NE. molb) THEN !in unit cell 0,0,0 but other molecules
                        place_1(:) = ions%new_car(na,:)
                        place_2(:) = ions%new_car(nb,:)
                        dist = sqrt( (place_1(1)-place_2(1))**2 + (place_1(2)-place_2(2))**2 + (place_1(3)-place_2(3))**2 )
								IF (dist .LE. min_dist) THEN
WRITE(*,*) '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
WRITE(*,*) '!! WARNING  Very small interatomic distance between 2 molecules detected (distance <', min_dist*0.52918,'Angstrom) !!'
WRITE(*,*) '!! WARNING  The C6 and C12 approximation for the van der Waals energy fails at such small distances !!'
WRITE(*,*) '!! WARNING  The interaction is between molecule', mola,' and molecule', molb, 'in unitcell ', ucx, ucy, ucz
WRITE(*,*) '!! WARNING  DO NOT USE this intermolecular interaction and its symmetric equivalents !!'
WRITE(*,*) '!! WARNING  The program does not ignore this value automatically, substract it from the total energies yourself !!'
WRITE(*,*) '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
								ENDIF
                        vdwE = -(C6/(dist**6))
                        repE = C12/(dist**12)
                        inter_nr = ((uc_nr-1) * nr_mol2) + ((mola-1) * nr_of_mol) + molb
                        inter_nr_sym = ((uc_nr-1) * nr_mol2) + ((molb-1) * nr_of_mol) + mola
                        ! A to B:
                        coulombE%c6(inter_nr) = coulombE%c6(inter_nr) + 0.5*vdwE
                        coulombE%c12(inter_nr) = coulombE%c12(inter_nr) + 0.5*repE
                        coulombE%core_core(inter_nr) = coulombE%core_core(inter_nr) + 0.5*(interaction/dist)
                        ! B to A:
                        coulombE%c6(inter_nr_sym) = coulombE%c6(inter_nr_sym) + 0.5*vdwE
                        coulombE%c12(inter_nr_sym) = coulombE%c12(inter_nr_sym) + 0.5*repE
                        coulombE%core_core(inter_nr_sym) = coulombE%core_core(inter_nr_sym) + 0.5*(interaction/dist)               
                     ENDIF !ucz .NE. 0 or ucy .NE. 0 or ucx .NE. 0
                  ENDDO !ucx
               ENDDO !ucy
            ENDDO !ucz
         ENDDO !nb
      ENDDO !na

      write(*,*) '------------------------------------------'
      write(*,*) 'Total C6-interaction: ', sum(coulombE%c6(:)), 'hartree'
      write(*,*) 'Total C12-interaction',sum(coulombE%c12(:)), 'hartree'
      write(*,*) 'core-core Coulomb interaction: ', sum(coulombE%core_core(:)), 'hartree'
      write(*,*) '------------------------------------------'

END SUBROUTINE core_core

END MODULE interactions_mod
