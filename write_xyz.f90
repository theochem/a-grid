!-----------------------------------------------------------------------------------------------
	   SUBROUTINE write_xyz_files(ions, ang2au, nr_molecules)
		TYPE(ions_obj), INTENT(IN) :: ions
		REAL(q2), INTENT(IN) :: ang2au
		INTEGER, INTENT(IN) :: nr_molecules
    	INTEGER :: i,j, XYZ, atoms_in_mol
    	CHARACTER(len=20) :: file_name
    	CHARACTER(len=6) :: mol_nr 
		CHARACTER(len=2) :: atom_name

		XYZ = 101 !which unit to write to
		DO i = 1,nr_molecules !loop over molecules
			atoms_in_mol = 0
   	   DO j = 1, ions%nions !loop over atoms
   	      IF (ions%molecule(j) .EQ. i) THEN !atom is in molecule
   	         atoms_in_mol = atoms_in_mol + 1
   	      ENDIF
   	   ENDDO

   	   WRITE(mol_nr,'(I6)') i
   	   file_name = 'molecule_'//TRIM(ADJUSTL(mol_nr))//'.xyz'
   	   OPEN(UNIT=XYZ, FILE=file_name, STATUS="REPLACE")
   	   WRITE(XYZ,*) atoms_in_mol !nr of atoms
			WRITE(XYZ,*) 'Contains one of the molecules detected by q-GRID, coordinates in Angstrom.'
! Still have to write the name of the atom....
   	   DO j = 1, ions%nions !loop over atoms
   	      IF (ions%molecule(j) .EQ. i) THEN !atom is in molecule
					CALL atom_nr2name(ions%atomic_num(j) ,atom_name)
   	         WRITE(XYZ,*) atom_name, ions%new_car(j,:)/ang2au
   	      ENDIF
   	   ENDDO
   	   CLOSE(XYZ)
   	ENDDO

   	file_name = 'all_molecules.xyz'
   	OPEN(UNIT=XYZ, FILE=file_name, STATUS="REPLACE")
   	WRITE(XYZ,*) ions%nions 
		WRITE(XYZ,*) 'Contains all of the molecules detected by q-GRID, coordinates in Angstrom.' 
	  	DO j = 1, ions%nions !loop over atoms
			CALL atom_nr2name(ions%atomic_num(j) ,atom_name)
   	   WRITE(XYZ,*) atom_name, ions%new_car(j,:)/ang2au
   	ENDDO
   	CLOSE(XYZ)

      END SUBROUTINE write_xyz_files
!-----------------------------------------------------------------------------
		SUBROUTINE atom_nr2name(atom_nr, atom_name)
			INTEGER, INTENT(IN) :: atom_nr
			CHARACTER(len=2), INTENT(OUT) :: atom_name

          IF (atom_nr .EQ. 1) THEN
             atom_name = 'H'
          ELSEIF (atom_nr .EQ. 2) THEN
             atom_name = 'He'
          ELSEIF (atom_nr .EQ. 3) THEN
             atom_name = 'Li'
          ELSEIF (atom_nr .EQ. 4) THEN
             atom_name = 'Be'
          ELSEIF (atom_nr .EQ. 5) THEN
             atom_name = 'B'
          ELSEIF (atom_nr .EQ. 6) THEN
             atom_name = 'C'
          ELSEIF (atom_nr .EQ. 7) THEN
             atom_name = 'N'
          ELSEIF (atom_nr .EQ. 8) THEN
             atom_name = 'O'
          ELSEIF (atom_nr .EQ. 9) THEN
             atom_name = 'F'
          ELSEIF (atom_nr .EQ. 10) THEN
             atom_name = 'Ne'
          ELSEIF (atom_nr .EQ. 11) THEN
             atom_name = 'Na'
          ELSEIF (atom_nr .EQ. 12) THEN
             atom_name = 'Mg'
          ELSEIF (atom_nr .EQ. 13) THEN
             atom_name = 'Al'
          ELSEIF (atom_nr .EQ. 14) THEN
             atom_name = 'Si'
          ELSEIF (atom_nr .EQ. 15) THEN
             atom_name = 'P'
          ELSEIF (atom_nr .EQ. 16) THEN
             atom_name = 'S'
          ELSEIF (atom_nr .EQ. 17) THEN
             atom_name = 'Cl'
          ELSEIF (atom_nr .EQ. 18) THEN
             atom_name = 'Ar'
          ELSE 
				 atom_name = 'XX'
             write(*,*) 'ERROR! Unknown atom! Only the atoms up to Argon are in the code.'
             write(*,*) 'ERROR! Look in functions_mol_mod.f90 and add this atom to the code.'
          ENDIF

		END SUBROUTINE atom_nr2name
!---------------------------------------------------------------------------
