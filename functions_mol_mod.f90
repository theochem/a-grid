! Written by Niek de Klerk, 2014, at the Radboud University Nijmegen. 
! With help from Joost van den Ende

! q-GRID and Bader are free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! A copy of the GNU General Public License is available at
! http://www.gnu.org/licenses/

!---------------------------------------------------------
! Contains (most of) the functions called by molecules_mod
!---------------------------------------------------------
MODULE functions_mol_mod

      USE kind_mod
      USE ions_mod
      USE charge_mod
      USE bader_mod

      TYPE coulomb_E
         INTEGER, ALLOCATABLE,DIMENSION(:)  :: mol1,mol2,ucz,ucy,ucx
         REAL(q2), ALLOCATABLE,DIMENSION(:) :: distx,disty,distz,dist
         REAL(q2), ALLOCATABLE,DIMENSION(:) :: core_core,core_grid,grid_grid,totalE,c6,c12,energy_recv
      END TYPE

      TYPE chg_row
         INTEGER, ALLOCATABLE,DIMENSION(:) :: mol,atom
         REAL(q2), ALLOCATABLE,DIMENSION(:)    :: e_charge
         REAL(q2), ALLOCATABLE,DIMENSION(:,:) :: pos_car
      END TYPE     

      CONTAINS
!-----------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------
           SUBROUTINE write_xyz_files(ions, ang2au, nr_molecules)
                TYPE(ions_obj), INTENT(IN) :: ions
                REAL(q2), INTENT(IN) :: ang2au
                INTEGER, INTENT(IN) :: nr_molecules
        INTEGER :: i,j, XYZ, atoms_in_mol
        CHARACTER(len=20) :: file_name
        CHARACTER(len=6) :: mol_nr 
                CHARACTER(len=2) :: atom_name

                XYZ = 101 !which unit to write to
                DO i = 1,nr_molecules !loop over molecules
                        atoms_in_mol = 0
           DO j = 1, ions%nions !loop over atoms
              IF (ions%molecule(j) .EQ. i) THEN !atom is in molecule
                 atoms_in_mol = atoms_in_mol + 1
              ENDIF
           ENDDO

           WRITE(mol_nr,'(I6)') i
           file_name = 'molecule_'//TRIM(ADJUSTL(mol_nr))//'.xyz'
           OPEN(UNIT=XYZ, FILE=file_name, STATUS="REPLACE")
           WRITE(XYZ,*) atoms_in_mol !nr of atoms
                        WRITE(XYZ,*) 'Contains one of the molecules detected by q-GRID, coordinates in Angstrom.'
! Still have to write the name of the atom....
           DO j = 1, ions%nions !loop over atoms
              IF (ions%molecule(j) .EQ. i) THEN !atom is in molecule
                                        CALL atom_nr2name(ions%atomic_num(j) ,atom_name)
                 WRITE(XYZ,*) atom_name, ions%new_car(j,:)/ang2au
              ENDIF
           ENDDO
           CLOSE(XYZ)
        ENDDO

        file_name = 'all_molecules.xyz'
        OPEN(UNIT=XYZ, FILE=file_name, STATUS="REPLACE")
        WRITE(XYZ,*) ions%nions 
                WRITE(XYZ,*) 'Contains all of the molecules detected by q-GRID, coordinates in Angstrom.' 
                DO j = 1, ions%nions !loop over atoms
                        CALL atom_nr2name(ions%atomic_num(j) ,atom_name)
           WRITE(XYZ,*) atom_name, ions%new_car(j,:)/ang2au
        ENDDO
        CLOSE(XYZ)

      END SUBROUTINE write_xyz_files
!-----------------------------------------------------------------------------
                SUBROUTINE atom_nr2name(atom_nr, atom_name)
                        INTEGER, INTENT(IN) :: atom_nr
                        CHARACTER(len=2), INTENT(OUT) :: atom_name

          IF (atom_nr .EQ. 1) THEN
             atom_name = 'H'
          ELSEIF (atom_nr .EQ. 2) THEN
             atom_name = 'He'
          ELSEIF (atom_nr .EQ. 3) THEN
             atom_name = 'Li'
          ELSEIF (atom_nr .EQ. 4) THEN
             atom_name = 'Be'
          ELSEIF (atom_nr .EQ. 5) THEN
             atom_name = 'B'
          ELSEIF (atom_nr .EQ. 6) THEN
             atom_name = 'C'
          ELSEIF (atom_nr .EQ. 7) THEN
             atom_name = 'N'
          ELSEIF (atom_nr .EQ. 8) THEN
             atom_name = 'O'
          ELSEIF (atom_nr .EQ. 9) THEN
             atom_name = 'F'
          ELSEIF (atom_nr .EQ. 10) THEN
             atom_name = 'Ne'
          ELSEIF (atom_nr .EQ. 11) THEN
             atom_name = 'Na'
          ELSEIF (atom_nr .EQ. 12) THEN
             atom_name = 'Mg'
          ELSEIF (atom_nr .EQ. 13) THEN
             atom_name = 'Al'
          ELSEIF (atom_nr .EQ. 14) THEN
             atom_name = 'Si'
          ELSEIF (atom_nr .EQ. 15) THEN
             atom_name = 'P'
          ELSEIF (atom_nr .EQ. 16) THEN
             atom_name = 'S'
          ELSEIF (atom_nr .EQ. 17) THEN
             atom_name = 'Cl'
          ELSEIF (atom_nr .EQ. 18) THEN
             atom_name = 'Ar'
          ELSE 
                                 atom_name = 'XX'
             write(*,*) 'ERROR! Unknown atom! Only the atoms up to Argon are in the code.'
             write(*,*) 'ERROR! Look in functions_mol_mod.f90 and add this atom to the code.'
          ENDIF

                END SUBROUTINE atom_nr2name
!---------------------------------------------------------------------------

      SUBROUTINE determine_dipole(chg,tot_points_now,ions)
	! Determines the dipole of the unit cell
      TYPE(chg_row), INTENT(IN) :: chg
      TYPE(ions_obj), INTENT(IN) :: ions
      INTEGER, INTENT(IN) :: tot_points_now
      REAL(q2), DIMENSION(:):: dipole(3), cell_middle(3)
      INTEGER :: i        

      dipole(:) = 0
      DO i = 1,3
         cell_middle(i) = 0.5*sum(ions%lattice(:,i))
      ENDDO
   ! loop over atoms
      DO i = 1,ions%nions
         dipole(:) = dipole(:) + (ions%new_car(i,:) - cell_middle(:)) * ions%charge(i) 
      ENDDO
   ! loop over gridpoints
      DO i = 1, tot_points_now
         dipole(:) = dipole(:) + (chg%pos_car(i,:) - cell_middle(:))*(-chg%e_charge(i)) !-chg?                 
      ENDDO

      write(*,*) 'Dipole of the unitcell, in atomic units (in x-, y-, and z- direction) is:'
      write(*,*) dipole(:)

      END SUBROUTINE determine_dipole        

!---------------------------------------------------------------------------------

      SUBROUTINE banana_check(zz,yy,xx,n1,n2,n3,npts,combining,chg,max_dist)
! Checks whether there is a point of another molecule between the gridpoints to be combined.
! Only for grouping of 5x5x5 !
      TYPE(charge_obj),INTENT(IN) :: chg
      INTEGER,INTENT(IN) :: zz,yy,xx,n1,n2,n3
      INTEGER,DIMENSION(3),INTENT(IN) :: npts
      LOGICAL,INTENT(INOUT) :: combining
      REAL(q2),INTENT(IN) :: max_dist

      INTEGER :: i,j,k
      INTEGER,DIMENSION(2,2,2,3) :: check

! Make all the possibilities that need to be checked:
      IF (zz .LT. -1) THEN
         check(:,:,:,3) = n3 - 1
      ELSEIF (zz .GT. 1) THEN
         check(:,:,:,3) = n3 + 1
      ELSEIF (abs(zz) .EQ. 1) THEN
         check(:,:,1,3) = n3
         check(:,:,2,3) = n3 + zz
      ELSE
         check(:,:,:,3) = n3
      ENDIF
      IF (yy .LT. -1) THEN
         check(:,:,:,2) = n2 - 1
      ELSEIF (yy .GT. 1) THEN
         check(:,:,:,2) = n2 + 1
      ELSEIF (abs(yy) .EQ. 1) THEN
         check(:,1,:,2) = n2
         check(:,2,:,2) = n2 + yy 
      ELSE
         check(:,:,:,2) = n2
      ENDIF
      IF (xx .LT. -1) THEN
         check(:,:,:,1) = n1 - 1
      ELSEIF (xx .GT. 1) THEN
         check(:,:,:,1) = n1 + 1
      ELSEIF (abs(xx) .EQ. 1) THEN
         check(1,:,:,1) = n1
         check(2,:,:,1) = n1 + xx 
      ELSE
         check(:,:,:,1) = n1
      ENDIF
 
      DO i = 1,2
         DO j = 1,2
            DO k = 1,2
! Periodic Boundary Conditions: 
               IF (check(i,j,k,3) .GT. npts(3)) THEN
                  check(i,j,k,3) = check(i,j,k,3) - npts(3)
               ELSEIF (check(i,j,k,3) .LT. 1 ) THEN
                  check(i,j,k,3) = check(i,j,k,3) + npts(3)
               ENDIF
               IF (check(i,j,k,2) .GT. npts(2)) THEN
                  check(i,j,k,2) = check(i,j,k,2) - npts(2)
               ELSEIF (check(i,j,k,2) .LT. 1 ) THEN
                  check(i,j,k,2) = check(i,j,k,2) + npts(2)
               ENDIF
               IF (check(i,j,k,1) .GT. npts(1)) THEN
                  check(i,j,k,1) = check(i,j,k,1) - npts(1)
               ELSEIF (check(i,j,k,1) .LT. 1 ) THEN
                  check(i,j,k,1) = check(i,j,k,1) + npts(1)
               ENDIF
! Check to which molecules the points in between belong:
               IF ( chg%molecule(n1,n2,n3) .NE. chg%molecule(check(i,j,k,1),check(i,j,k,2),check(i,j,k,3)) ) THEN !Don't combine
                  combining = .FALSE.
               ELSEIF ( sqrt((chg%pos_car(n1,n2,n3,1)-chg%pos_car(check(i,j,k,1),check(i,j,k,2),check(i,j,k,3),1))**2 + & 
                  &      (chg%pos_car(n1,n2,n3,2)-chg%pos_car(check(i,j,k,1),check(i,j,k,2),check(i,j,k,3),2))**2 + &
                  &      (chg%pos_car(n1,n2,n3,3)-chg%pos_car(check(i,j,k,1),check(i,j,k,2),check(i,j,k,3),3))**2)  & 
                  & .GT. max_dist) THEN !Check whether the distance is ok, if not:
                  combining = .FALSE.
               ENDIF
            ENDDO
         ENDDO
      ENDDO

      END SUBROUTINE banana_check

!-----------------------------------------------------------------------------------------------------------

      SUBROUTINE group_molecules_minimise(ions,mol_center,nr_of_mol)
! Places molecules as close to each other as possible to find all the interactions
      TYPE(ions_obj),INTENT(INOUT) :: ions
      REAL(q2), DIMENSION(:,:),INTENT(INOUT) :: mol_center
      INTEGER,INTENT(OUT) :: nr_of_mol

      REAL(q2) :: dist,tmp_dist
      INTEGER :: x,y,z,cellx,celly,cellz,i,j,k,counter
      LOGICAL :: changed
      REAL(q2),DIMENSION(3) :: ij_vector, tmp_mol_center  

! write out original positions:
      DO i = 1,nr_of_mol
         write(*,*) 'Molecule ', i, 'contains ', int(mol_center(i,4)), 'atoms. Center at: ', mol_center(i,1:3)
      ENDDO

      counter = 0
      changed = .TRUE.
      DO WHILE(changed) !Check until no molecule has moved anymore
         changed = .FALSE.
         DO i = 1,nr_of_mol
            dist = 10000
            DO z = -1,1,1  !loop over all neighbouring unit cells
               DO y = -1,1,1
                  DO x = -1,1,1
                     tmp_dist = 0
                     tmp_mol_center(:) = 0
         tmp_mol_center(1) = mol_center(i,1) + (x*ions%lattice(1,1) + y*ions%lattice(2,1) + z*ions%lattice(3,1)) !x
         tmp_mol_center(2) = mol_center(i,2) + (x*ions%lattice(1,2) + y*ions%lattice(2,2) + z*ions%lattice(3,2)) !y
         tmp_mol_center(3) = mol_center(i,3) + (x*ions%lattice(1,3) + y*ions%lattice(2,3) + z*ions%lattice(3,3)) !z
                     DO j = 1,nr_of_mol
                        IF (j .NE. i) THEN !calculate distances between the molecules
                           DO k = 1,3
                              ij_vector(k) = tmp_mol_center(k) - mol_center(j,k)
                           ENDDO
                           tmp_dist = tmp_dist + sqrt( ij_vector(1)**2 + ij_vector(2)**2 + ij_vector(3)**2 )
                        ENDIF
                     ENDDO
                     IF ( tmp_dist .LT. dist) THEN  !save closest distance and in which direction it is
                        dist = tmp_dist
                        cellx = x  
                        celly = y
                        cellz = z
                     ELSEIF (((abs(tmp_dist - dist) .LE. 0.01) .AND. (x .EQ. 0) .AND. (y .EQ. 0) .AND. (z .EQ. 0))) THEN 
                     !If the distances are practically the same, chose unit cell 0,0,0
                        dist = tmp_dist
                        cellx = 0
                        celly = 0
                        cellz = 0
                     ENDIF
                  ENDDO !x
               ENDDO !y 
            ENDDO !z
            IF ((cellx .NE. 0) .OR. (celly .NE. 0) .OR. (cellz .NE. 0)) THEN !move molecule i
               changed = .TRUE.
               mol_center(i,1) = 0 !reset values
               mol_center(i,2) = 0                 
               mol_center(i,3) = 0
               DO k = 1,ions%nions !Move all the atoms in molecule i along the vector
                  IF (i .EQ. ions%molecule(k)) THEN
         ions%new_car(k,1) = ions%new_car(k,1) + (cellx*ions%lattice(1,1) + celly*ions%lattice(2,1) + cellz*ions%lattice(3,1)) !x
         ions%new_car(k,2) = ions%new_car(k,2) + (cellx*ions%lattice(1,2) + celly*ions%lattice(2,2) + cellz*ions%lattice(3,2)) !y
         ions%new_car(k,3) = ions%new_car(k,3) + (cellx*ions%lattice(1,3) + celly*ions%lattice(2,3) + cellz*ions%lattice(3,3)) !z
                     mol_center(i,1) = mol_center(i,1) + ions%new_car(k,1) 
                     mol_center(i,2) = mol_center(i,2) + ions%new_car(k,2)
                     mol_center(i,3) = mol_center(i,3) + ions%new_car(k,3)
                  ENDIF
               ENDDO
               mol_center(i,1) = mol_center(i,1)/mol_center(i,4) !calculate mol_center again
               mol_center(i,2) = mol_center(i,2)/mol_center(i,4)
               mol_center(i,3) = mol_center(i,3)/mol_center(i,4)
               write(*,*) 'Molecule', i, 'moved along vector',cellx,celly,cellz
            ENDIF !cellx NE 0, ....
         ENDDO !i
         counter = counter + 1
      ENDDO
       
      IF (counter .GT. 1) THEN !If something has moved write out the new positions, and possibly a warning.
         IF (counter .GT. 2) THEN
            write(*,*) 'Warning! Molecules had to be moved multiple times, carefully check positions!'
         ENDIF
         DO i = 1,nr_of_mol
            write(*,*) 'Molecule ', i, 'contains ', int(mol_center(i,4)), 'atoms. Center now at: ', mol_center(i,1:3)
         ENDDO
      ENDIF

      END SUBROUTINE group_molecules_minimise

!---------------------------------------------------------------------------------

      SUBROUTINE combine_points(npts,chg,tot_points_now,lattice,tot_grouped,fast_calc)
! Group gridpoints which belong to the same molecule to decrease the number of interactions which need to be calculated
! this is done to speed-up calculations, the new position of the gridpoints is based on their weighted average.
      TYPE(charge_obj),INTENT(INOUT) :: chg
      INTEGER,DIMENSION(3),INTENT(IN) :: npts
      INTEGER, INTENT(OUT) :: tot_points_now,tot_grouped
      REAL(q2), DIMENSION(3,3),INTENT(IN) :: lattice
      LOGICAL, INTENT(IN) :: fast_calc

      INTEGER :: j,n1,n2,n3,zz,z,ucz,yy,y,ucy,xx,x,ucx,cube_size,cube_dist
      INTEGER :: not_combined,grouped,i,far_apart
      LOGICAL :: combining
      REAL(q2) :: distx,disty,distz,sum_e,x_new,y_new,z_new,dist,max_dist,max_grid_dist

      grouped = 1
      tot_grouped = 0
      tot_points_now = 0
      not_combined = 0
      far_apart = 0

      IF (fast_calc) THEN
         cube_size = 5  !Size of the cube which is to be grouped.
         cube_dist = 2  !Distance in gridpoints in all directions
         write(*,*) 'Fast calculation selected, grouping gridpoints in a 5x5x5 cube'
      ELSE
         cube_size = 3  !Size of the cube which is to be grouped.
         cube_dist = 1  !Distance in gridpoints in all directions
         write(*,*) 'Normal calculation selected, grouping gridpoints in a 3x3x3 cube'
      ENDIF

      ! Group only up to a maximum distance apart, first the distance in which the gridpoints are furthest apart:
      max_grid_dist = 0
      DO i = 1,3
         dist = lattice(i,i)/real(npts(i))
         IF (dist .GT. max_grid_dist) THEN
            max_grid_dist = dist
         ENDIF
      ENDDO
      !determine max_dist to group:
      max_dist = SQRT(3*(REAL(cube_dist)*max_grid_dist)**2)
      !old: max_dist = max_dist*4*real(cube_dist) !Max distance within which still to group        

      DO j = 1,cube_size !Start at j=1 and go upto cube_size
         DO n3 = j, npts(3),cube_size !loop over gridpoints, with increments of cube_size.
            DO n2 = j, npts(2),cube_size 
               DO n1 = j, npts(1),cube_size 
!If gridpoint in the middle of the cube is not empty (0 = reassigned already)  
                  IF ((chg%e_charge(n1,n2,n3) .NE. 0) ) THEN               
                     sum_e = chg%e_charge(n1,n2,n3)
                     x_new = chg%pos_car(n1,n2,n3,1)*sum_e
                     y_new = chg%pos_car(n1,n2,n3,2)*sum_e
                     z_new = chg%pos_car(n1,n2,n3,3)*sum_e
                     DO zz = -cube_dist,cube_dist  !cube_dist from middle of cubes.
                        z = n3 + zz
                        ucz = 0
                        IF ( z .GT. npts(3)) THEN !Periodic Boundary Conditions
                           z = z - npts(3)
                           ucz = +1
                        ELSEIF ( z .LT. 1) THEN
                           z = z + npts(3)
                           ucz = -1
                        ENDIF
                        DO yy = -cube_dist,cube_dist
                           y = n2 + yy
                           ucy = 0
                           IF ( y .GT. npts(2)) THEN !Periodic Boundary Conditions
                              y = y - npts(2)
                              ucy = +1
                           ELSEIF ( y .LT. 1) THEN
                              y = y + npts(2) 
                              ucy = -1
                           ENDIF
                           DO xx = -cube_dist,cube_dist
                              x = n1 + xx
                              ucx = 0
                              IF ( x .GT. npts(1)) THEN !Periodic Boundary Conditions    
                                 x = x - npts(1)
                                 ucx = +1
                              ELSEIF ( x .LT. 1) THEN
                                 x = x + npts(1)
                                 ucx = -1 
                              ENDIF
              !IF (in the same molecule) AND (not the middle gridpoint) AND (not reassigned already)
                              IF ( (chg%molecule(n1,n2,n3) .EQ. chg%molecule(x,y,z)) .AND. & 
                                 & ((xx .NE. 0) .OR. (yy .NE. 0) .OR. (zz .NE. 0)) .AND. (chg%e_charge(x,y,z) .NE. 0) ) THEN
                                 combining = .TRUE.
                                 IF (abs(zz) .GT. 1 .OR. abs(yy) .GT. 1 .OR. abs(xx) .GT. 1) THEN
                                    CALL banana_check(zz,yy,xx,n1,n2,n3,npts,combining,chg,max_dist) !check whether there is another molecule in between the gridpoints
                                    IF (.NOT. combining) THEN
                                       not_combined = not_combined + 1
                                    ENDIF
                                 ENDIF
                                 IF (combining) THEN
!  Combine the positions of the points (weighed average), no PBC! This is already done in function mol_contig.
                                    distx = chg%pos_car(x,y,z,1) !x
                                    disty = chg%pos_car(x,y,z,2) !y
                                    distz = chg%pos_car(x,y,z,3) !z
                             ! Check the distance between the points, must be smaller (or equal) as max_dist:
                                    IF (sqrt( (distx-chg%pos_car(n1,n2,n3,1))**2 + (disty-chg%pos_car(n1,n2,n3,2))**2 & 
                                         &   + (distz-chg%pos_car(n1,n2,n3,3))**2 ) .LE. max_dist) THEN
                                       x_new = x_new + (distx * chg%e_charge(x,y,z)) !combine based on weighed average 
                                       y_new = y_new + (disty * chg%e_charge(x,y,z)) 
                                       z_new = z_new + (distz * chg%e_charge(x,y,z))
                                       grouped = grouped + 1
                                       sum_e = sum_e + chg%e_charge(x,y,z) !sum of the charge of all combined gridpoints
                                       chg%e_charge(n1,n2,n3) = chg%e_charge(n1,n2,n3) + chg%e_charge(x,y,z)
                                       chg%e_charge(x,y,z) = 0  ! set e_charge in x,y,z to zero
                                    ELSE
                                       far_apart = far_apart + 1 ! counter if distance is to far away
                                    ENDIF
                                 ENDIF !combining
                              ENDIF  
                           ENDDO !xx
                        ENDDO !yy
                     ENDDO !zz
                     !Divide the x,y,z coordinates by the total charge of the combined points to get the weighed average position
                     chg%pos_car(n1,n2,n3,1) = x_new/sum_e
                     chg%pos_car(n1,n2,n3,2) = y_new/sum_e
                     chg%pos_car(n1,n2,n3,3) = z_new/sum_e
                     tot_grouped = tot_grouped + grouped -1
                     grouped = 1 
                  ENDIF !chg%e_charge .NE. 0
               ENDDO !n1
            ENDDO !n2
         ENDDO !n3
      ENDDO !j

     ! Determine how many points there are now
      DO n3 = 1, npts(3)
         DO n2 = 1, npts(2)
            DO n1 = 1, npts(1)
               IF (chg%e_charge(n1,n2,n3) .NE. 0 ) THEN
                  tot_points_now = tot_points_now + 1
               ENDIF
            ENDDO  
         ENDDO                
      ENDDO !n1
      write(*,*) 'Number of points after combining is: ',tot_points_now, & 
			& 'Number of points was: ',npts(1)*npts(2)*npts(3), 'Number of points combined: ', tot_grouped

      END SUBROUTINE combine_points

!----------------------------------------------------------------------------------------------------

      SUBROUTINE mol_contig_chg(chg,ions,npts)
! Move the gridpoints as close to the atom it is assigned to as possible, to make the molecule contiguous.
      TYPE(charge_obj),INTENT(INOUT) :: chg
      TYPE(ions_obj),INTENT(INOUT) :: ions
      INTEGER,DIMENSION(3), INTENT(IN) :: npts

      INTEGER :: n1,n2,n3, moved,counter
      INTEGER :: ucx,ucy,ucz
      REAL(q2) :: difx,dify,difz,x_temp,y_temp,z_temp,x_min,y_min,z_min
      REAL(q2) :: difx_temp,dify_temp,difz_temp,dist_temp,dist
      LOGICAL :: change

      moved = 0

      write(*,'(2X,A,$)') 'Making molecules contiguous: '
      DO n3 = 1,npts(3)
         DO n2 = 1,npts(2)
            DO n1 = 1,npts(1)
               counter = 0
               dist_temp = 100
               change = .FALSE.
               x_min = chg%pos_car(n1,n2,n3,1) 
               y_min = chg%pos_car(n1,n2,n3,2)
               z_min = chg%pos_car(n1,n2,n3,3)
               difx = ions%new_car(chg%atom(n1,n2,n3),1) - chg%pos_car(n1,n2,n3,1)                
               dify = ions%new_car(chg%atom(n1,n2,n3),2) - chg%pos_car(n1,n2,n3,2)
               difz = ions%new_car(chg%atom(n1,n2,n3),3) - chg%pos_car(n1,n2,n3,3)
               dist = sqrt(difx**2 + dify**2 + difz**2) !original distance between atom and gridpoint
               DO ucx = -2,2 !check at 2 unit cells distance because the atoms could be moved already.
                  DO ucy = -2,2
                     DO ucz = -2,2
                        IF ( (ucx .NE. 0) .OR. (ucy .NE. 0) .OR. (ucz .NE. 0) ) THEN 
                    x_temp = chg%pos_car(n1,n2,n3,1) + (ucx*ions%lattice(1,1) + ucy*ions%lattice(2,1) + ucz*ions%lattice(3,1)) !x
                    y_temp = chg%pos_car(n1,n2,n3,2) + (ucx*ions%lattice(1,2) + ucy*ions%lattice(2,2) + ucz*ions%lattice(3,2)) !y
                    z_temp = chg%pos_car(n1,n2,n3,3) + (ucx*ions%lattice(1,3) + ucy*ions%lattice(2,3) + ucz*ions%lattice(3,3)) !z
                           difx_temp = ions%new_car(chg%atom(n1,n2,n3),1) - x_temp
                           dify_temp = ions%new_car(chg%atom(n1,n2,n3),2) - y_temp
                           difz_temp = ions%new_car(chg%atom(n1,n2,n3),3) - z_temp
                           dist_temp = sqrt(difx_temp**2 + dify_temp**2 + difz_temp**2)
                           IF (dist_temp .LT. dist) THEN !if distance at this unit cell is less than the original distance
                              change = .TRUE.
                              x_min = x_temp
                              y_min = y_temp
                              z_min = z_temp
                              dist = dist_temp
                           ENDIF !dist_temp .LT. dist
                        ENDIF
                     ENDDO !ucz
                  ENDDO !ucy
               ENDDO !ucx
               IF (change) THEN
                  moved = moved + 1
                  IF (x_min .NE. chg%pos_car(n1,n2,n3,1)) THEN !new place if movement was in x-direction
                     chg%pos_car(n1,n2,n3,1) = x_min
                  ENDIF
                  IF (y_min .NE. chg%pos_car(n1,n2,n3,2)) THEN !new place if movement was in y-direction
                     chg%pos_car(n1,n2,n3,2) = y_min
                  ENDIF
                  IF (z_min .NE. chg%pos_car(n1,n2,n3,3)) THEN !new place if movement was in z-direction
                     chg%pos_car(n1,n2,n3,3) = z_min
                  ENDIF
               ENDIF !change    
        ! Calculate new distance for checking if everything went ok.
               difx = ions%new_car(chg%atom(n1,n2,n3),1) - chg%pos_car(n1,n2,n3,1)                
               dify = ions%new_car(chg%atom(n1,n2,n3),2) - chg%pos_car(n1,n2,n3,2)
               difz = ions%new_car(chg%atom(n1,n2,n3),3) - chg%pos_car(n1,n2,n3,3)
               IF ((abs(difx/ions%lattice(1,1)) .GT. 0.5) .OR. (abs(dify/ions%lattice(2,2)) .GT. 0.5) &
                 &  .OR. (abs(difz/ions%lattice(3,3)) .GT. 0.5)) THEN
                  write(*,*) 'ERROR! point is to far away from atom!', difx/ions%lattice(1,1),dify/ions%lattice(2,2), & 
								&  difz/ions%lattice(3,3)
               ENDIF
            ENDDO !n1
         ENDDO
         IF (mod(n3,20) .EQ. 0) THEN         
            write(*,'(A,$)') '*'
         ENDIF
      ENDDO!n3
      write(*,*) 'Moved points: ',moved

      END SUBROUTINE mol_contig_chg

!!-----------------------------------------------------------------------------------------------
      SUBROUTINE find_molecules(ions,mol_center,nr_of_mol)
! Constructs molecules ONLY based on covalent radii of the atoms and their positions.
      TYPE(ions_obj),INTENT(INOUT) :: ions
      REAL(q2),ALLOCATABLE,DIMENSION(:,:),INTENT(OUT) :: mol_center
      INTEGER,INTENT(OUT) :: nr_of_mol

      TYPE(ions_obj) :: tmp_ions
      INTEGER :: i,j,k,mol_before,ucz,ucy,ucx,counter, h_atom, other_atom
      LOGICAL :: pbc_used,first_atom, mol_correct
      REAL(q2) :: dist,distx,disty,distz
      INTEGER,ALLOCATABLE,DIMENSION(:) :: mol_nr_before,mol_nr_after,nr_bonds
      INTEGER,ALLOCATABLE,DIMENSION(:,:) :: bonded_to
      REAL(q2), ALLOCATABLE, DIMENSION(:,:) :: bond_dist
      LOGICAL, ALLOCATABLE, DIMENSION(:) :: correct

 ! give each atom it's own molecule number to start with:
      DO i = 1,ions%nions
         ions%molecule(i) = i
      ENDDO

      ALLOCATE(nr_bonds(ions%nions))
      ALLOCATE(bonded_to(ions%nions,10))
      ALLOCATE(bond_dist(ions%nions,10))
      nr_bonds(:) = 0
      bonded_to(:,:) = 0
      bond_dist(:,:) = 999
	! Find molecules
      DO i = 1,(ions%nions-1) !loop over all atom-atom combinations:
         DO j = i+1, ions%nions         
            DO ucz = -1,1 ! Loop over all neighbouring cells
               DO ucy = -1,1
                  DO ucx = -1,1
                     distx = (ucx*ions%lattice(1,1) + ucy*ions%lattice(2,1) + ucz*ions%lattice(3,1)) !x
                     disty = (ucx*ions%lattice(1,2) + ucy*ions%lattice(2,2) + ucz*ions%lattice(3,2)) !y
                     distz = (ucx*ions%lattice(1,3) + ucy*ions%lattice(2,3) + ucz*ions%lattice(3,3)) !z
                     dist = sqrt( ((ions%r_car(j,1)+distx)-ions%r_car(i,1))**2 + ((ions%r_car(j,2)+disty)-ions%r_car(i,2))**2 &
                        &  + ((ions%r_car(j,3)+distz)-ions%r_car(i,3))**2 ) !dist = sqrt(x + y + z)  
        !  IF: atom-atom distance < covalent radius --> same molecule          
                     IF (dist .LE. (ions%cov_rad(i) + ions%cov_rad(j))) THEN
                        nr_bonds(i) = nr_bonds(i) + 1 
                        nr_bonds(j) = nr_bonds(j) + 1                
                        bonded_to(i,nr_bonds(i)) = j
                        bonded_to(j,nr_bonds(j)) = i
                        bond_dist(i,nr_bonds(i)) = dist
                        bond_dist(j,nr_bonds(j)) = dist
                        IF ( (ions%atomic_num(i) .NE. 1 .OR. nr_bonds(i) .LT. 2) .AND. & 
                        & (ions%atomic_num(j) .NE. 1 .OR. nr_bonds(j) .LT. 2) ) THEN !check whether hydrogen atoms have more than 1 bond
                           mol_before = ions%molecule(j)
                           ions%molecule(j) = ions%molecule(i)
                           DO k = 1, ions%nions !give all atoms belonging to mol_before the new molecule number (i)
                              IF (ions%molecule(k) .EQ. mol_before) THEN
                                 ions%molecule(k) = ions%molecule(i)
                              ENDIF
                           ENDDO !k
                        ELSE !hydrogen has more as 1 bond, find the closest atom 
                           IF (ions%atomic_num(i) .EQ. 1) THEN
                              h_atom = i  
                              other_atom = j
                           ELSE
                              h_atom = j
                              other_atom = i
                           ENDIF
                           WRITE(*,*) '!! WARNING !!!!!!!!! WARNING !!!!!!! WARNING !!!!!!!! WARNING !!!!!!!!!!'
                           WRITE(*,*) '!! Based on the covalent radii a hydrogen was bonded to two atoms !'
                           WRITE(*,*) '!! Atom ', h_atom, 'was bonded to atoms ', bonded_to(h_atom,1), & 
                                        & 'and ', bonded_to(h_atom,2)
                           WRITE(*,*) '!! The distances are ', bond_dist(h_atom,1)/1.8897, 'and ', bond_dist(h_atom,2)/1.8897, & 
                                        & 'Angstrom, repectively'            
                           WRITE(*,*) '!! Check your input coordinates! Unless you expect very strong & 
								 				 & hydrogen bonds in your system!'
                           WRITE(*,*) '!! Will assign the H-atom to the same molecule as the atom it is closest to, & 
                                        & and continue the calculation'                         
                           WRITE(*,*) '!! WARNING !!!!!!!!! WARNING !!!!!!! WARNING !!!!!!!! WARNING !!!!!!!!!!'
      !Assign the h_atom to the molecule of the atom which is closest to it, after that remove the data about the second bond:
                           IF (bond_dist(h_atom,1) .LT. bond_dist(h_atom,2) ) THEN !h_atom is already assigned to the right molecule
                              !bonded_to(other_atom,nr_bonds(other_atom)) = 0 !?how to also correctly remove this for the elseif case...?
                              !bond_dist(other_atom,nr_bonds(other_atom)) = 999
                              nr_bonds(other_atom) = nr_bonds(other_atom) - 1 
                              bonded_to(h_atom,2) = 0
                              bond_dist(h_atom,2) = 999
                              nr_bonds(h_atom) = nr_bonds(h_atom) - 1 
                           ELSEIF (bond_dist(h_atom,1) .GT. bond_dist(h_atom,2)) THEN
             !assign h_atom to the molecule of the second bond, i.e. other_atom
                              ions%molecule(h_atom) = ions%molecule(other_atom)
                              nr_bonds(h_atom) = nr_bonds(h_atom) - 1 
                              nr_bonds(bonded_to(h_atom,1)) = nr_bonds(bonded_to(h_atom,1)) - 1 
                              bonded_to(h_atom,1) = bonded_to(h_atom,2)
                              bond_dist(h_atom,1) = bond_dist(h_atom,2)                   
                              bonded_to(h_atom,2) = 0
                              bond_dist(h_atom,2) = 999
                           ENDIF !
                        ENDIF 
    				      ENDIF !cov_rad
				      ENDDO !ucx
	            ENDDO !ucy
		      ENDDO !ucz
	      ENDDO !j
	   ENDDO !i
     
       !Check whether atoms have more as 4 bonds, and give a warning if this is the case 
      DO i = 1,ions%nions
         IF (nr_bonds(i) .GT. 4) THEN
            WRITE(*,*) '!! WARNING !!!!!!!!! WARNING !!!!!!! WARNING !!!!!!!! WARNING !!!!!!!!!!'
            WRITE(*,*) '!! Atom ', i, "has more than 4 bonds! It's element nr. is: ", ions%atomic_num(i) 
            WRITE(*,*) '!! Please check whether this is correct !!'  
            WRITE(*,*) '!! WARNING !!!!!!!!! WARNING !!!!!!! WARNING !!!!!!!! WARNING !!!!!!!!!!'
         ENDIF
      ENDDO

      DEALLOCATE(nr_bonds)
      DEALLOCATE(bonded_to)
      DEALLOCATE(bond_dist)

  ! Determine number of molecules and number them from 1 to nr_of_mol:
      ALLOCATE(mol_nr_before(ions%nions))
      ALLOCATE(mol_nr_after(ions%nions))
      ALLOCATE(tmp_ions%molecule(ions%nions))
      mol_nr_before(:) = 0
      mol_nr_after(:) = 0
      tmp_ions%molecule(:) = 0

      nr_of_mol = 0
      DO i = 1,ions%nions
         IF (ions%molecule(i) .EQ. i) THEN
            nr_of_mol = nr_of_mol + 1
         ENDIF
      ENDDO
      write(*,*) 'Number of molecules found: ',nr_of_mol

   ! number the molecules from 1 to nr_of_mol
      nr_of_mol = 0
      DO i = 1,ions%nions
         IF (ions%molecule(i) .EQ. i) THEN
            nr_of_mol = nr_of_mol + 1
            mol_nr_before(nr_of_mol) = ions%molecule(i)
            mol_nr_after(nr_of_mol) = nr_of_mol
         ENDIF
      ENDDO

   ! assign the atoms to the molecules numbered 1 to nr_of_mol
      DO i = 1,ions%nions
         DO j = 1,nr_of_mol
            IF (ions%molecule(i) .EQ. mol_nr_before(j)) THEN
               tmp_ions%molecule(i) = mol_nr_after(j)
            ENDIF
         ENDDO
         ions%molecule(i) = tmp_ions%molecule(i)
      ENDDO

      DEALLOCATE(mol_nr_before)
      DEALLOCATE(mol_nr_after)
      DEALLOCATE(tmp_ions%molecule)  

	! move atoms to make the molecules connected
	  ALLOCATE(correct(ions%nions))
      pbc_used = .TRUE.
      counter = 0
     	DO WHILE (pbc_used)
      	pbc_used = .FALSE.
		   correct(:) = .FALSE.
      	counter = counter + 1
			DO i = 1, nr_of_mol  !loop over molecules
				first_atom = .TRUE. 
				mol_correct = .FALSE.
				DO WHILE(.NOT. mol_correct)
					mol_correct = .TRUE.
					DO j = 1, ions%nions !loop over atoms, first encountered atom in the molecule is the 'reference'
						IF ( ions%molecule(j) .EQ. i) THEN
							IF (first_atom) THEN ! at correct position by definition.
								first_atom = .FALSE.
								correct(j) = .TRUE.
								CALL neighbour_atoms_right_pos(ions, i, j, correct, pbc_used) !ions,molecule,atom, correct, pbc_used
							ELSEIF (correct(j)) THEN !already at the correct position, check the neigbbours
								CALL neighbour_atoms_right_pos(ions, i, j, correct, pbc_used) !ions,mol,atom, correct, pbc_used
							ELSE !After reaching the last atom, start a new loop over the atoms, because every atom must have correct(j) = TRUE before exiting the loop over this molecule
								mol_correct = .FALSE. 
							ENDIF !first_atom
						ENDIF !molecule(j) .EQ. i
					ENDDO !j
				ENDDO !Do while not mol_correct
			ENDDO !i
         write(*,*) 'Molecule search round:',counter
      ENDDO !do while pbc_used      
      ions%new_car(:,:) = ions%r_car(:,:) !save new atom positions in new_car
	  DEALLOCATE(correct)

 ! 4) Determine the cartesian center of the molecule
      ALLOCATE(mol_center(nr_of_mol,4))
      mol_center(:,:) = 0
      DO i = 1,nr_of_mol
         DO j = 1,ions%nions
            IF (ions%molecule(j) .EQ. i) THEN
               DO k = 1,3
                  mol_center(i,k) = mol_center(i,k) + ions%r_car(j,k)
               ENDDO
               mol_center(i,4) = mol_center(i,4) + 1
            ENDIF
         ENDDO
         DO k = 1,3
            mol_center(i,k) = mol_center(i,k)/mol_center(i,4)
         ENDDO
      ENDDO

      END SUBROUTINE find_molecules

!--------------------------------------------------------------------------------------------------

		SUBROUTINE neighbour_atoms_right_pos(ions,mol,atom, correct, pbc_used)
 ! Check whether the neigbour atoms are at the right position (only used in find_molecules)
		TYPE(ions_obj), INTENT(INOUT) :: ions
		INTEGER, INTENT(IN) :: mol, atom
		LOGICAL, INTENT(INOUT), DIMENSION(:) :: correct
		LOGICAL, INTENT(INOUT) :: pbc_used
 	   INTEGER :: i, ucz,ucx,ucy
		REAL(q2) :: distx, disty, distz, dist

		DO i = 1,ions%nions
			IF (ions%molecule(i) .EQ. mol .AND. (.NOT. correct(i))) THEN !in the same molecule and not at the right position already
		 	   DO ucz = -2,2 !2) Loop over all neighbouring cells
               DO ucy = -2,2
                  DO ucx = -2,2
                     distx = (ucx*ions%lattice(1,1) + ucy*ions%lattice(2,1) + ucz*ions%lattice(3,1)) !x
                     disty = (ucx*ions%lattice(1,2) + ucy*ions%lattice(2,2) + ucz*ions%lattice(3,2)) !y
                     distz = (ucx*ions%lattice(1,3) + ucy*ions%lattice(2,3) + ucz*ions%lattice(3,3)) !z
                     dist = SQRT( ((ions%r_car(i,1)+distx)-ions%r_car(atom,1))**2 + & 
									& ((ions%r_car(i,2)+disty)-ions%r_car(atom,2))**2 + ((ions%r_car(i,3)+distz)-ions%r_car(atom,3))**2 ) !dist = sqrt(x + y + z)  
                     IF (dist .LE. (ions%cov_rad(i) + ions%cov_rad(atom))) THEN         !  IF: atom-atom distance < covalent radius  
							   IF ( (ABS(ucz) + ABS(ucy) + ABS(ucx)) .EQ. 0 ) THEN !position is correct
							 	   correct(i) = .TRUE.
							   ELSE ! not in cell 0,0,0 --> move atom 
                           ions%r_car(i,1) = ions%r_car(i,1) + distx
                           ions%r_car(i,2) = ions%r_car(i,2) + disty
                           ions%r_car(i,3) = ions%r_car(i,3) + distz
							      pbc_used = .TRUE.
							      correct(i) = .TRUE.
							   ENDIF
						   ENDIF
				  	   ENDDO !ucx
					ENDDO !ucy
				ENDDO !ucz
		   ENDIF
		ENDDO !i

		END SUBROUTINE neighbour_atoms_right_pos

!-----------------------------------------------------------------------------------------------

	SUBROUTINE cart2frac(lat, cart_coor, frac_coor)
! Changes cartesian to fractional coordinates using Cramer's rule:
	REAL(q2), DIMENSION(:,:), INTENT(IN) :: lat(3,3) !lattice
	REAL(q2), DIMENSION(:), INTENT(IN) :: cart_coor(3)
	REAL(q2), DIMENSION(:), INTENT(OUT) :: frac_coor(3)
	REAL(q2) :: det_lat,det_x, det_y, det_z

	det_lat = lat(1,1)*lat(2,2)*lat(3,3) + lat(1,2)*lat(2,3)*lat(3,1) + lat(1,3)*lat(2,1)*lat(3,2) & 
	& - lat(1,1)*lat(2,3)*lat(3,2) - lat(1,2)*lat(2,1)*lat(3,3) - lat(1,3)*lat(2,2)*lat(3,1) !determinant of the lattice

	det_x = cart_coor(1)*lat(2,2)*lat(3,3) + cart_coor(2)*lat(2,3)*lat(3,1) + cart_coor(3)*lat(2,1)*lat(3,2) & 
	& - cart_coor(1)*lat(2,3)*lat(3,2) - cart_coor(2)*lat(2,1)*lat(3,3) - cart_coor(3)*lat(2,2)*lat(3,1)

	det_y = lat(1,1)*cart_coor(2)*lat(3,3) + lat(1,2)*cart_coor(3)*lat(3,1) + lat(1,3)*cart_coor(1)*lat(3,2) & 
	& - lat(1,1)*cart_coor(3)*lat(3,2) - lat(1,2)*cart_coor(1)*lat(3,3) - lat(1,3)*cart_coor(2)*lat(3,1)

	det_z = lat(1,1)*lat(2,2)*cart_coor(3) + lat(1,2)*lat(2,3)*cart_coor(1) + lat(1,3)*lat(2,1)*cart_coor(2) & 
	& - lat(1,1)*lat(2,3)*cart_coor(2) - lat(1,2)*lat(2,1)*cart_coor(3) - lat(1,3)*lat(2,2)*cart_coor(1)

	!coordinates in a,b,c:
	frac_coor(1) = det_x/det_lat !a
	frac_coor(2) = det_y/det_lat !b
	frac_coor(3) = det_z/det_lat !c

	END SUBROUTINE cart2frac

!---------------------------------------------------------------------------------------------------------------------------

      SUBROUTINE write_interaction_files(coulombE,nr_inter_mol,nr_of_mol,mol_center,lattice,title)
!  Writes the interaction energies between molecules in several files
!  - molecule_interactions.dat: only the total energy and the distance
!  - mol_interactions_long.dat: all the contributions (C6, C12 and Coulomb), the total energy and the distance
!  - coulomb_interactions.dat: all the separate coulomb-contributions (core-core, core-grid and grid-grid)

      TYPE(coulomb_E)  :: coulombE
      REAL(q2), DIMENSION(3,3),INTENT(IN) :: lattice
      REAL(q2), DIMENSION(:,:),INTENT(IN) :: mol_center
      CHARACTER(LEN=128), INTENT(IN) :: title

      INTEGER :: i,j,nr_inter_mol,nr_of_mol
      REAL(q2) :: E_total, hrtr2kcal,hrtr2kcalmol,hrtr2kj,hrtr2kjmol,e_coulomb,ang2au 
      REAL(q2) :: a,b,c,alpha,beta,gama
      REAL(q2), DIMENSION(3) :: vector_a,vector_b,vector_c
      REAL(q2), ALLOCATABLE, DIMENSION(:,:) :: mol_frac

      hrtr2kcal = 627.509469 !conversion factor from hartree to kcal/mol
      hrtr2kcalmol = 627.509469/(nr_of_mol*2) !conversion factor from sum(everthing) in hartree/(mol unitcells) to kcal/(mol molecules)
      hrtr2kj = 2625.49962 !conversion factor from hartree to kJ/mol
      hrtr2kjmol = 2625.49962/(nr_of_mol*2) !conversion factor from sum(everthing) in hartree/(mol unitcells) to kJ/(mol molecules)
      ang2au = 1.889726133921252 !conversion factor from angstrom to atomic units   
      ALLOCATE(mol_frac(nr_of_mol,3))

      open(100,file='molecule_interactions.dat',status='replace',action='write')
      open(101,file='mol_interactions_long.dat',status='replace',action='write')
      open(102,file='coulomb_interactions.dat',status='replace',action='write')   

      DO j = 100,102
         write(j,*) '# E-density file used for calculations= ', title
         write(j,*) '# Delimiter= white-space'
      ENDDO

      write(100,*) '# Description= This file contains the interaction-energy between molecule 1 in unitcell (0,0,0) & 
                    & and molecule 2 in unitcell (A,B,C)'
      write(101,*) '# Description= This file contains the interaction-energy between molecule 1 in unitcell (0,0,0) & 
                    & and molecule 2 in unitcell (A,B,C)'
      write(102,*) '# Description= This file contains the Coulomb interactions between molecule 1 in unitcell (0,0,0) & 
                    & and molecule 2 in unitcell (A,B,C)'  
      write(102,*) '#'
      write(102,*) '#!! WARNING !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
      write(102,*) '#!! WARNING: Energies in this file are in Hartree/(mol interactions) instead of kJ/(mol interactions)'
      write(102,*) '#!! WARNING: The conversion factor from Hartree to kJ is: Hartree *', hrtr2kj, '= kJ '
      write(102,*) '#!! WARNING !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'

      DO j = 100,102
         write(j,*) '#'
         write(j,*) '# --------------------------------------------------------------------------------------------'
      ENDDO

      a = sqrt( lattice(1,1)**2 + lattice(1,2)**2 + lattice(1,3)**2 )
      b = sqrt( lattice(2,1)**2 + lattice(2,2)**2 + lattice(2,3)**2 )
      c = sqrt( lattice(3,1)**2 + lattice(3,2)**2 + lattice(3,3)**2 )

! Calculate the angles of the unit cell:
      DO i = 1,3
         vector_a(i) = lattice(1,i) 
         vector_b(i) = lattice(2,i)
         vector_c(i) = lattice(3,i)
         !write(*,*) 'lattice', lattice(:,i)
      ENDDO 
      alpha = ACOS( DOT_PRODUCT(vector_b,vector_c)/(b*c) ) !in radians
      beta = ACOS( DOT_PRODUCT(vector_a,vector_c)/(a*c) )
      gama = ACOS( DOT_PRODUCT(vector_a,vector_b)/(a*b) )

! Transform for mol_center the cartesian coordinates to fractional coordinates: 
		DO i = 1,nr_of_mol
			CALL cart2frac(lattice,mol_center(i,:),mol_frac(i,:))
		ENDDO

      E_total = sum(coulombE%totalE(:))*hrtr2kjmol
      write(100,'(A, F10.3, A)') ' # total E=', E_total,' kJ/mol, (0.5*sum of all the interaction energies= Lattice energy)'

      write(101,'(A, F10.3, A)') ' # C6-interaction=        ', sum(coulombE%c6(:))*hrtr2kj, ' kJ/(mol unitcells), & 
												& bonds counted double'
      write(101,'(A, F10.3, A)') ' # C12-interaction=       ', sum(coulombE%c12(:))*hrtr2kj, ' kJ/(mol unitcells), & 
											& bonds counted double'                 
      write(101,'(A, F10.3, A)') ' # Coulomb-interaction=   ', (sum(coulombE%core_core(:)) + sum(coulombE%core_grid(:)) &
      &    +sum(coulombE%grid_grid(:)))*hrtr2kj, ' kJ/(mol unitcells), bonds counted double'   
      write(101,'(A, F10.3, A)') ' # Total-interaction-E=   ',( sum(coulombE%c6(:))+sum(coulombE%c12(:))+ & 
      & sum(coulombE%core_core(:))+ sum(coulombE%core_grid(:))+sum(coulombE%grid_grid(:)))*hrtr2kj, ' kJ/(mol unitcells),& 
      &  bonds counted double'          
      write(101,'(A, F10.3, A)') ' # --------------------------------------------------------------------------------------------'
      write(101,'(A, F10.3, A)') ' # Total-energy=          ',sum(coulombE%totalE(:))*hrtr2kjmol,' kJ/mol, & 
      & (0.5*sum of all the interaction energies= Lattice energy)' 

      e_coulomb = (sum(coulombE%core_core(:)) + sum(coulombE%core_grid(:)) + sum(coulombE%grid_grid(:)))/(nr_of_mol*2)
      write(102,'(A, F10.3, A)') ' # Total-Coulomb-E=', e_coulomb,'Hartree/mol, (0.5*sum of all the coulombic interaction energies)'

      DO j = 100,102
         write(j,*) '# --------------------------------------------------------------------------------------------'
         write(j,*) '#'
         write(j,*) '# Lattice vectors: Base lengths= [a,b,c] (in Angstrom), Base angles= [alpha, beta, gamma] (in degrees)' 
         write(j,'(A, 3F10.3, A)') ' # Base lengths= [',a/ang2au, b/ang2au, c/ang2au,']'
         write(j,'(A, 3F10.3, A)') ' # Base angles= [',(180/pi)*alpha, (180/pi)*beta, (180/pi) *gama,']'
         write(j,*) ''
         write(j,*) ''
         write(j,*) '# Sites::'
         write(j,*) '# Atomic mean position= The coordinates are based on the mean of the positions of the atoms in a molecule.'
         write(j,*) '# $1= nr = Ordinal number of the site (i.e. a molecule/growth unit or where a molecule/growth unit can be)'
         write(j,*) '# $2= x  = a-coordinate of the atomic mean position of the site in the unit cell. Fractional coordinates.'
         write(j,*) '# $3= y  = b-coordinate of the atomic mean position of the site in the unit cell. Fractional coordinates.'
         write(j,*) '# $4= z  = c-coordinate of the atomic mean position of the site in the unit cell. Fractional coordinates.'
         write(j,*) '# nr            x            y            z' 
         DO i = 1,nr_of_mol
            write(j,'(I3, 3F12.6)') i, mol_frac(i,1:3)
         ENDDO
      ENDDO

      DO j = 100,102
         write(j,*) ''
         write(j,*) ''
         write(j,*) '# Bonds::'
         write(j,*) '# $1= nr1         = Site number of the first site joined by the bond.'!Molecule 1' 
         write(j,*) '# $2= nr2         = Site number of the second site joined by the bond.'!Molecule 2'
         write(j,*) '# $3= cellA       = Unit cell that contains site 2 along A-direction'
         write(j,*) '# $4= cellB       = Unit cell that contains site 2 along B-direction'
         write(j,*) '# $5= cellC       = Unit cell that contains site 2 along C-direction'
      ENDDO

      write(100,*) '# $6= EkJ         = Interaction energy between the two molecules  unit= kJ/(mol interactions)'
      write(100,*) '# $7= Dist        = Distance between the centers of the molecules  unit= Angstrom'
      write(100,*) '# -------------------------------------------------------------------------------------------'
      write(100,*) '# nr1 ','nr2  ','cellA    ','cellB    ','cellC    ','EkJ', 'Dist'

      write(101,*) '# $6= C6          = C6-interaction energy between the two molecules  unit= kJ/(mol interactions)'
      write(101,*) '# $7= C12         = C12-interaction energy between the two molecules  unit= kJ/(mol interactions)'
      write(101,*) '# $8= Coulomb     = Coulomb interactions between cores and gridpoints  unit= kJ/(mol interactions)'
      write(101,*) '# $9= EkJ         = Total interaction energy between the two molecules  unit= kJ/(mol interactions)'
      write(101,*) '# $10= Dist       = Distance between the centers of the molecules  unit= Angstrom'
      write(101,*) '# --------------------------------------------------------------------------------------------'
      write(101,*) '# nr1 ','nr2  ','cellA ','cellB ','cellC ','C6 ','C12 ','Coulomb ', 'EkJ ', 'Dist'

      write(102,*) '# $6= CC          = Coulomb interactions between the cores of the molecules  unit= hartree/(mol interactions)'
      write(102,*) '# $7= GG          = Coulomb interactions between cores and gridpoints of the molecules & 
							&  unit= hartree/(mol unitcells)'
      write(102,*) '# $8= CG          = Coulomb interactions between gridpoints of the molecules  unit= hartree/(mol interactions)'
      write(102,*) '# $9= Coulomb     = Coulomb energy between the two molecules  unit= hartree/(mol interactions)'
      write(102,*) '# $10= Dist       = Distance between the centers of the molecules  unit= Angstrom'
      write(102,*) '# --------------------------------------------------------------------------------------------'
      write(102,*) '# nr1 ','nr2  ','cellA ','cellB ','cellC ','CC ','GG ','CG ','Coulomb ','Dist'
      
      DO i = 1,nr_inter_mol 
         !mol_interactions: writes 5 integers, followed by 2 floats up to 5 digits after the dot.  
         write(100,'(5I5,2F15.6)') coulombE%mol1(i), coulombE%mol2(i), coulombE%ucx(i), coulombE%ucy(i), coulombE%ucz(i), &
         &  (coulombE%c6(i) + coulombE%c12(i) + coulombE%core_core(i) + coulombE%core_grid(i) + coulombE%grid_grid(i))*hrtr2kj, &
         &  (coulombE%dist(i)/ang2au)

         !mol_interactions_long: writes 5 integers, followed by 7 floats up to 5 digits after the dot. 
         write(101,'(5I5,5F15.6)') coulombE%mol1(i), coulombE%mol2(i), coulombE%ucx(i), coulombE%ucy(i), coulombE%ucz(i), &
         & coulombE%c6(i)*hrtr2kj,coulombE%c12(i)*hrtr2kj, (coulombE%core_core(i)+coulombE%grid_grid(i)+ &
         & coulombE%core_grid(i))*hrtr2kj, coulombE%totalE(i)*hrtr2kj, (coulombE%dist(i)/ang2au)

         !coulomb_interactions: writes 5 integers, followed by 5 floats up to 5 digits after the dot. 
         write(102,'(5I5,5F15.6)') coulombE%mol1(i), coulombE%mol2(i), coulombE%ucx(i), coulombE%ucy(i), coulombE%ucz(i), &
         & coulombE%core_core(i), coulombE%grid_grid(i), coulombE%core_grid(i), (coulombE%core_core(i)+coulombE%grid_grid(i)+ &
         & coulombE%core_grid(i)), (coulombE%dist(i)/ang2au)
      ENDDO

      close(100)
      close(101)
      close(102)        

      DEALLOCATE(mol_frac)

      ! write the results to the screen:
      write(*,*) '# -------------------------------------------------------'
      write(*,'(A, F10.3, A)') '# C6 interaction=     ', sum(coulombE%c6(:))*hrtr2kj, ' kJ/(mol unitcells), bonds counted twice' 
      write(*,'(A, F10.3, A)') '# C12 interaction=    ', sum(coulombE%c12(:))*hrtr2kj, ' kJ/(mol unitcells), bonds counted twice'
      write(*,'(A, F10.3, A)') '# Coulomb interaction=', (sum(coulombE%core_core(:)) + sum(coulombE%core_grid(:)) &
      &    + sum(coulombE%grid_grid(:))) * hrtr2kj, ' kJ/(mol unitcells), bonds counted twice'  
      write(*,'(A, F10.3, A)') '# Total energy=       ',( sum(coulombE%c6(:))+sum(coulombE%c12(:))+sum(coulombE%core_core(:))+ & 
      &    sum(coulombE%core_grid(:))+sum(coulombE%grid_grid(:)))*hrtr2kj, ' kJ/(mol unitcells), bonds counted twice'          
      write(*,*) '# -------------------------------------------------------'
      write(*,'(A, F10.3, A)') '# Total interaction energy=', sum(coulombE%totalE(:))*hrtr2kjmol, ' kJ/mol' 
      write(*,*) '# -------------------------------------------------------'

      END SUBROUTINE write_interaction_files
!--------------------------------------------------------------------------------------------------------------------------------------

END MODULE functions_mol_mod
