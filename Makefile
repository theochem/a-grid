.SUFFIXES: .f90

FC = mpif90 

FFLAGS = -O3 -c -Wall -Wextra -Wtabs -fimplicit-none #for debugging 
#FFLAGS = -O3 -c #for normal use

LINK = #-static #-glibc

OBJS = kind_mod.o       \
       matrix_mod.o     \
       ions_mod.o       \
       options_mod.o    \
       charge_mod.o     \
       chgcar_mod.o     \
       cube_mod.o       \
       io_mod.o         \
       bader_mod.o      \
       voronoi_mod.o    \
       multipole_mod.o  \
       atoms_mod.o      \
       functions_mol_mod.o \
       interactions_mod.o \
       molecules_mod.o


%.o %.mod : %.f90
	$(FC) $(FFLAGS)  $*.f90

qgrid: $(OBJS) main.o
	rm -f qgrid
	$(FC) $(LINK) main.o -o $@ $(OBJS) 

dist: qgrid
	tar -cf qgrid_lnx_64.tar qgrid
	gzip -9 qgrid_lnx_64.tar

clean:
	rm -f *.o *.mod qgrid qgrid_lnx_64.tar.gz
