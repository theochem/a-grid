
README belonging to qGRID-v1
____________________________________________________________
TERMS OF USE: 

cite orginal paper: 

q-GRID: A New Method To Calculate Lattice and Interaction Energies for Molecular Crystals from Electron Densities
N. J. J. de Klerk, J. A. van den Ende, R. Bylsma, P. GranÄiÄ, G. A. Wijs, H. M. Cuppen, and H. Meekes
Crystal Growth & Design, 16, 662 (2016)  

doi: 10.1021/acs.cgd.5b01164

____________________________________________________________

COMPILE and RUN:

To compile the program simply type: 

        make

in this folder, this will generate .o- and .mod-files and the executable (qgrid).  

The program can be run with the command:

    mpirun -np <# processors> qgrid chargefile

The only required input argument is the name of the charge density file. It will automatically determine the rest of the things it needs to know.

Optional arguments are -fast for combining 5x5x5 grid points and -cells cella cellb cellz for taking different numbers than 1 unit cell in all directions
e.g.: mpirun -np 4 qgrid -fast -cells 2 4 3 CHGCAR 
does a calculation by 4 processors with grouping of grid points
takes 2 cells in -a and 2 cells in +a, 4 cells in -b and 4 cells +b, 3 cells in -c and 3 cells +c direction

Help can be called with the command: 

         qgrid -h

This will display all the possible options, and describes how to use them.


