! Written by Niek de Klerk, 2014, at the Radboud University Nijmegen. 

! q-GRID and Bader are free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! A copy of the GNU General Public License is available at
! http://www.gnu.org/licenses/

!---------------------------------------------------------
! Contains the atomic data needed for the calculation of interactions. 
! Only implemented up to Ar (element 18)
!---------------------------------------------------------
MODULE atoms_mod

        USE kind_mod

        TYPE atom_data
         REAL(q2), DIMENSION(18)  :: c6,vdw_dist,cov_rad 
         INTEGER, DIMENSION(18) :: atomic_number
        END TYPE

        CONTAINS
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! c6 and vdw_dist from: S. Grimme: 
! Semiempirical GGA-type Density Functional Constructe with a Long-range dispersion Corrections (2006)
! UNITS: c6 in J*nm^6*mol^-1, vdw_dist in Angstrom
!
! Covalent radii from Crystal09 manual, in Angstrom.
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        SUBROUTINE get_atom_data(atoms_data)

        TYPE(atom_data), INTENT(OUT) :: atoms_data
        INTEGER :: i

        DO i = 1,18
           atoms_data%atomic_number(i) = i
        ENDDO

        !H
        atoms_data%c6(1) = 0.14
        atoms_data%vdw_dist(1) = 1.001 
        atoms_data%cov_rad(1) = 0.68 !angstrom !0.68

        !He
        atoms_data%c6(2) = 0.08
        atoms_data%vdw_dist(2) = 1.012
        atoms_data%cov_rad(2) = 1.47

        !Li
        atoms_data%c6(3) = 1.61
        atoms_data%vdw_dist(3) = 0.825
        atoms_data%cov_rad(3) = 1.65

        !Be
        atoms_data%c6(4) = 1.61
        atoms_data%vdw_dist(4) = 1.408
        atoms_data%cov_rad(4) = 1.18

        !B
        atoms_data%c6(5) = 3.13
        atoms_data%vdw_dist(5) = 1.485
        atoms_data%cov_rad(5) = 0.93

        !C
        atoms_data%c6(6) = 1.75
        atoms_data%vdw_dist(6) = 1.452
        atoms_data%cov_rad(6) = 0.81

        !N
        atoms_data%c6(7) = 1.23
        atoms_data%vdw_dist(7) = 1.397
        atoms_data%cov_rad(7) = 0.78

        !O
        atoms_data%c6(8) = 0.70
        atoms_data%vdw_dist(8) = 1.342
        atoms_data%cov_rad(8) = 0.78

        !F
        atoms_data%c6(9) = 0.75
        atoms_data%vdw_dist(9) = 1.287 
        atoms_data%cov_rad(9) = 0.76

        !Ne
        atoms_data%c6(10) = 0.63
        atoms_data%vdw_dist(10) = 1.243
        atoms_data%cov_rad(10) = 1.68

        !Na
        atoms_data%c6(11) = 5.71
        atoms_data%vdw_dist(11) = 1.144
        atoms_data%cov_rad(11) = 2.01

        !Mg
        atoms_data%c6(12) = 5.71
        atoms_data%vdw_dist(12) = 1.364
        atoms_data%cov_rad(12) = 1.57

        !Al
        atoms_data%c6(13) = 10.79
        atoms_data%vdw_dist(13) = 1.639
        atoms_data%cov_rad(13) = 1.50

        !Si
        atoms_data%c6(14) = 9.23
        atoms_data%vdw_dist(14) = 1.716
        atoms_data%cov_rad(14) = 1.23

        !P
        atoms_data%c6(15) = 7.84
        atoms_data%vdw_dist(15) = 1.705
        atoms_data%cov_rad(15) = 1.15

        !S
        atoms_data%c6(16) = 5.57
        atoms_data%vdw_dist(16) = 1.683 
        atoms_data%cov_rad(16) = 1.09

        !Cl
        atoms_data%c6(17) = 5.07
        atoms_data%vdw_dist(17) = 1.639
        atoms_data%cov_rad(17) = 1.05 

        !Ar
        atoms_data%c6(18) = 4.61
        atoms_data%vdw_dist(18) = 1.595
        atoms_data%cov_rad(18) = 1.97

        END SUBROUTINE get_atom_data

END MODULE atoms_mod
