! Written by Niek de Klerk, 2014, at the Radboud University Nijmegen. 
! With help from Joost van den Ende

! q-GRID and Bader are free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! A copy of the GNU General Public License is available at
! http://www.gnu.org/licenses/

!---------------------------------------------------------
! Calculates the interactions between molecules, and all the other things that are needed to do this
!---------------------------------------------------------
MODULE molecules_mod
      USE charge_mod
      USE kind_mod
      USE bader_mod
      USE ions_mod
      USE atoms_mod
      USE functions_mol_mod
      USE options_mod
      USE interactions_mod
      USE mpi

      IMPLICIT NONE

      CONTAINS
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      SUBROUTINE molecules(bdr,ions,chg,opts,nproc,myrank,ierror)
       
      TYPE(bader_obj)  :: bdr
      TYPE(ions_obj)   :: ions
      TYPE(charge_obj) :: chg
      TYPE(atom_data)  :: atoms_data
      TYPE(coulomb_E)  :: coulombE
      TYPE(chg_row) :: chg_new
      TYPE(options_obj) :: opts

      LOGICAL  :: only_valence_e,charge_diff
      INTEGER  :: n1,n2,n3,i,j,atom,cellz,celly,cellx,ucz,ucy,ucx
      INTEGER  :: inter_nr,nr_inter_mol,uc_nr,xper,yper,zper,nr_cellx,nr_celly,nr_cellz,nr_of_mol,nr_mol2
      INTEGER  :: mola,molb,molecule_nr,tot_combined,tot_points_now,unitcells,nr_of_points,total_core_charge,nr_of_atoms
      INTEGER, DIMENSION(3) :: npts
      REAL(q2) :: C6_conv,ang2au,distx,disty,distz,total_charge
      REAL(q2),ALLOCATABLE,DIMENSION(:) :: mol_core_chg,mol_charge
      REAL(q2),ALLOCATABLE,DIMENSION(:,:) :: mol_center
      !MPI STUFF:
      INTEGER,INTENT(IN) :: ierror, nproc,myrank
      INTEGER :: istatus

! Assign some constants to all processors: -----------------------------------------------------------
      ! Calculate the energy up to unit-cell: 
      CALL MPI_BCAST(opts%cells_dist,3,MPI_INTEGER,0,MPI_COMM_WORLD,istatus,ierror)
      cellz = opts%cells_dist(3)
      celly = opts%cells_dist(2)
      cellx = opts%cells_dist(1)

      nr_cellz = 2*cellz + 1
      nr_celly = 2*celly + 1
      nr_cellx = 2*cellx + 1
      unitcells = nr_cellz*nr_celly*nr_cellx !nr of unit cells to be calculated

      IF (myrank .EQ. 0) THEN
         write(*,*) "Calculating interactions up to", opts%cells_dist, 'unitcells distance.' 
      ENDIF
      ang2au = 1.889726133921252 !conversion factor from angstrom to atomic units      
      c6_conv = (0.001/2625.5)*((10*ang2au)**6) !J*nm^6/mol --> Hartree*au^6  
      total_core_charge = 0

      IF (myrank .EQ. 0) THEN
         npts(1) = chg%npts(1)
         npts(2) = chg%npts(2)
         npts(3) = chg%npts(3)
         nr_of_atoms = ions%nions        
         IF (opts%in_opt .NE. opts%in_cube) THEN  !For CHGCAR-files:
            write(*,*) 'CHGCAR-file'
            ions%lattice(:,:) = ions%lattice(:,:)*ang2au
            chg%lat2car(:,:) = chg%lat2car(:,:)*ang2au
         ELSE
			!WATCH OUT: Cube-file not really tested !
            write(*,*) 'CUBE-file'
         ENDIF
      ENDIF

      CALL MPI_BCAST(ions%lattice(:,:),9,MPI_REAL8,0,MPI_COMM_WORLD,istatus,ierror)
      CALL MPI_BCAST(npts(:),3,MPI_INTEGER,0,MPI_COMM_WORLD,istatus,ierror)
      CALL MPI_BCAST(nr_of_atoms,1,MPI_INTEGER,0,MPI_COMM_WORLD,istatus,ierror)

! Allocate some stuff: ------------------------------------------------ 
      ALLOCATE(chg%e_charge(npts(1),npts(2),npts(3)))
      ALLOCATE(chg%pos_car(npts(1),npts(2),npts(3),3))
      ALLOCATE(chg%atom(npts(1),npts(2),npts(3)))
      ALLOCATE(chg%molecule(npts(1),npts(2),npts(3)))
      chg%molecule(:,:,:) = 0
      chg%e_charge(:,:,:) = 0
      chg%pos_car(:,:,:,:) = 0
      chg%atom(:,:,:) = 0

      ALLOCATE(ions%c6(nr_of_atoms))
      ALLOCATE(ions%vdw_dist(nr_of_atoms))
      ALLOCATE(ions%molecule(nr_of_atoms))
      ALLOCATE(ions%new_car(nr_of_atoms,3))
      ALLOCATE(ions%cov_rad(nr_of_atoms))
      ALLOCATE(ions%charge(nr_of_atoms))
      IF (myrank .NE. 0) THEN
         ALLOCATE(ions%atomic_num(nr_of_atoms))
         ions%atomic_num(:) = 0       
      ENDIF
      ions%c6(:) = 0
      ions%vdw_dist(:) = 0
      ions%molecule(:) = 0
      ions%new_car(:,:) = 0
      ions%cov_rad(:) = 0        
		ions%charge(:) = 0

! Get the data of the atoms: 
      CALL MPI_BCAST(ions%atomic_num(:),nr_of_atoms,MPI_INTEGER,0,MPI_COMM_WORLD,istatus,ierror)
      CALL get_atom_data(atoms_data)
      DO i = 1,nr_of_atoms
         atom = ions%atomic_num(i)
         ions%c6(i) = C6_conv * atoms_data%c6(atom) !ions%c6 is in atomic units now
         ions%vdw_dist(i) = ang2au * atoms_data%vdw_dist(atom)
         ions%cov_rad(i) = ang2au * atoms_data%cov_rad(atom)
         total_core_charge = total_core_charge + atom 
      ENDDO

      IF (myrank .EQ. 0) THEN
         nr_of_points = npts(1)*npts(2)*npts(3)
         total_charge = 0
         write(*,*) 'Gridsize is: ', npts(1), npts(2), npts(3)

! Find molecules based on covalent radii:        
         write(*,*) 'Looking for molecules.....'
         CALL find_molecules(ions,mol_center,nr_of_mol)

! minimise the distances between molecules:
         CALL group_molecules_minimise(ions,mol_center,nr_of_mol) 
         CALL write_xyz_files(ions, ang2au, nr_of_mol)
      ENDIF !myrank .EQ. 0, info about molecules read in

      CALL MPI_BCAST(nr_of_mol,1,MPI_INTEGER,0,MPI_COMM_WORLD,istatus,ierror)
      nr_mol2 = nr_of_mol * nr_of_mol !(nr of mol)^2
      nr_inter_mol = nr_mol2 * unitcells !nr of interactions between molecules

! Allocate coulombE (stuff that depends on the nr of molecules):
      ALLOCATE(coulombE%mol1(nr_inter_mol)) 
      ALLOCATE(coulombE%mol2(nr_inter_mol)) 
      ALLOCATE(coulombE%ucz(nr_inter_mol)) 
      ALLOCATE(coulombE%ucy(nr_inter_mol)) 
      ALLOCATE(coulombE%ucx(nr_inter_mol)) 
      ALLOCATE(coulombE%c6(nr_inter_mol))
      ALLOCATE(coulombE%c12(nr_inter_mol))
      ALLOCATE(coulombE%core_core(nr_inter_mol))
      ALLOCATE(coulombE%core_grid(nr_inter_mol))
      ALLOCATE(coulombE%grid_grid(nr_inter_mol))
      ALLOCATE(coulombE%distx(nr_inter_mol))
      ALLOCATE(coulombE%disty(nr_inter_mol)) 
      ALLOCATE(coulombE%distz(nr_inter_mol))

! Initialise CoulombE-struct:
      coulombE%c6(:) = 0
      coulombE%c12(:) = 0
      coulombE%core_core(:) = 0
      coulombE%core_grid(:) = 0 
      coulombE%grid_grid(:) = 0

      DO mola = 1,nr_of_mol
         DO molb = 1,nr_of_mol
            DO ucz = -cellz,cellz
               zper = ucz + cellz + 1
               DO ucy = -celly,celly
                  yper = ucy + celly + 1    
                  DO ucx = -cellx,cellx
                     xper = ucx + cellx + 1 
                     uc_nr = zper + (yper-1)*nr_cellz + (xper-1)*nr_cellz*nr_celly
                     inter_nr = ((uc_nr-1) * nr_mol2) + ((mola-1) * nr_of_mol) + molb                   
                     coulombE%mol1(inter_nr) = mola
                     coulombE%mol2(inter_nr) = molb
                     coulombE%ucz(inter_nr)  = ucz
                     coulombE%ucy(inter_nr)  = ucy
                     coulombE%ucx(inter_nr)  = ucx
                     distx = (ucx*ions%lattice(1,1) + ucy*ions%lattice(2,1) + ucz*ions%lattice(3,1)) !x
                     disty = (ucx*ions%lattice(1,2) + ucy*ions%lattice(2,2) + ucz*ions%lattice(3,2)) !y
                     distz = (ucx*ions%lattice(1,3) + ucy*ions%lattice(2,3) + ucz*ions%lattice(3,3)) !z
                     coulombE%distx(inter_nr) = distx
                     coulombE%disty(inter_nr) = disty
                     coulombE%distz(inter_nr) = distz
                  ENDDO
               ENDDO 
            ENDDO 
         ENDDO !molb
      ENDDO !mola
!-----------------------------------------------------------------------------

      IF (myrank .EQ. 0) THEN
         write(*,*) 'Number of molecular interactions: ', nr_inter_mol
!       assign gridpoints to molecules and assign cartesian coordinates
         DO n3 = 1, npts(3)
            DO n2 = 1, npts(2)
               DO n1 = 1, npts(1) !Determine in which molecule each gridpoint is:
                  atom = bdr%nnion(bdr%volnum(n1,n2,n3))
                  chg%atom(n1,n2,n3) = atom
                  molecule_nr = ions%molecule(atom)
                  chg%molecule(n1,n2,n3) = molecule_nr
                  chg%e_charge(n1,n2,n3) = chg%rho(n1,n2,n3)/real(nr_of_points) !convert charge/volume to e-charge
                  total_charge = total_charge + chg%e_charge(n1,n2,n3)    
!Function value AT the gridpoint (origin at 0,0,0)
                  chg%pos_car(n1,n2,n3,1) = (n1-1)*chg%lat2car(1,1) + (n2-1)*chg%lat2car(1,2) + (n3-1)*chg%lat2car(1,3) !x, cartesian
                  chg%pos_car(n1,n2,n3,2) = (n1-1)*chg%lat2car(2,1) + (n2-1)*chg%lat2car(2,2) + (n3-1)*chg%lat2car(2,3) !y
                  chg%pos_car(n1,n2,n3,3) = (n1-1)*chg%lat2car(3,1) + (n2-1)*chg%lat2car(3,2) + (n3-1)*chg%lat2car(3,3) !z      
               ENDDO !n3
            ENDDO !n2         
         ENDDO !n1

! Determine if it is valence-e only or full electron:
         IF ( (total_core_charge - total_charge) .LE. 1.9) THEN !Less as 1.9 electrons difference --> full e-charge
            write(*,*) 'Full electron calculation, electron charge is:', total_charge, 'core charge is:', total_core_charge
            only_valence_e = .FALSE.
         ELSEIF ( (total_core_charge - total_charge) .GT. 1.9 ) THEN !More as 1.9 electrons difference --> valence only
            only_valence_e = .TRUE.
         ENDIF

         ALLOCATE(mol_core_chg(nr_of_mol))
         mol_core_chg(:) = 0
! Give the atoms the charge needed to calculate the coulomb interactions: 
         IF (myrank .EQ. 0) THEN
            IF (only_valence_e) THEN !change charge of atoms to valence-charge
               total_core_charge = 0
               DO i = 1,nr_of_atoms
                  IF ((ions%atomic_num(i) .GE. 1) .AND. (ions%atomic_num(i) .LE. 2)) THEN !H or He
                      ions%charge(i) = real(ions%atomic_num(i))
                  ELSEIF ((ions%atomic_num(i) .GE. 3) .AND. (ions%atomic_num(i) .LE. 10)) THEN !Li to Ne
                      ions%charge(i) = (real(ions%atomic_num(i)) - 2.0)  
                  ELSEIF ((ions%atomic_num(i) .GE. 11) .AND. (ions%atomic_num(i) .LE. 18)) THEN !Na to Ar
                     ions%charge(i) = (real(ions%atomic_num(i)) - 10.0)  
                  ELSE
                     write(*,*) 'Atom number is: ',ions%atomic_num(i), 'this list only goes to atom nr. 18!'
                     write(*,*) 'ERROR! Atom unknown, add it to this list (in molecules_mod.f90)'
                     write(*,*) 'and to the lists in atoms_mod.f90 and chgcar_mod.f90'       
                  ENDIF
                  mol_core_chg(ions%molecule(i)) = mol_core_chg(ions%molecule(i)) + ions%charge(i)
                  total_core_charge = total_core_charge + int(ions%charge(i))
               ENDDO
               write(*,*) 'Valence electron calculation, electron charge is:', total_charge, 'core charge is:', total_core_charge
            ELSE
               DO i = 1,nr_of_atoms
                  ions%charge(i) = real(ions%atomic_num(i))
                  mol_core_chg(ions%molecule(i)) = mol_core_chg(ions%molecule(i)) + ions%charge(i)
               ENDDO
            ENDIF
         ENDIF

! If the number of electrons is WRONG: ?Maybe it is better to just stop the calculation?
         IF (abs(total_charge - total_core_charge) .GT. 0.01) THEN 
			write(*,*) '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
            write(*,*) '!! ERROR !! The unit cell is not charge neutral!'
            write(*,*) '!! ERROR !! This makes the results VERY VERY unreliable!'
            write(*,*) '!! ERROR !! Electron charge:',total_charge,'core charge:',total_core_charge, & 
								& 'Difference:',(total_charge-total_core_charge)
            write(*,*) '!! ERROR !! Stopping the progam for this reason!'
		    write(*,*) '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
            STOP
         ENDIF

! Make molecules contiguous (aaneensluitend)
         CALL mol_contig_chg(chg,ions,npts)
      ENDIF !myrank = 0

! Broadcast some of the stuff just determined to all the processors: 
      CALL MPI_BCAST(only_valence_e,1,MPI_LOGICAL,0,MPI_COMM_WORLD,istatus,ierror)
      CALL MPI_BCAST(ions%charge(:),nr_of_atoms,MPI_REAL8,0,MPI_COMM_WORLD,istatus,ierror)    
      CALL MPI_BCAST(chg%molecule(:,:,:),npts(1)*npts(2)*npts(3),MPI_INTEGER,0,MPI_COMM_WORLD,istatus,ierror)
      CALL MPI_BCAST(chg%atom(:,:,:),npts(1)*npts(2)*npts(3),MPI_REAL8,0,MPI_COMM_WORLD,istatus,ierror)
      
      IF (myrank .EQ. 0) THEN !Determine the charge of each molecule:
         ALLOCATE(mol_charge(nr_of_mol))
         mol_charge(:) = 0
         DO n3 = 1, npts(3)
            DO n2 = 1, npts(2)
               DO n1 = 1, npts(1)
                  IF (abs(chg%e_charge(n1,n2,n3)) .GT. 0) THEN    
                     mol_charge(chg%molecule(n1,n2,n3)) = mol_charge(chg%molecule(n1,n2,n3)) + chg%e_charge(n1,n2,n3)
                  ENDIF
               ENDDO !n1
            ENDDO
         ENDDO !n3
         !Determine whether the molecules are charged...
         charge_diff = .FALSE.
         DO i = 1,nr_of_mol
            IF (abs(mol_core_chg(i) - mol_charge(i)) .GT. 0.1) THEN
               charge_diff = .TRUE.
					write(*,*) '! WARNING !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
	            write(*,*) '! WARNING !! Charged molecules, RESULTS PROBABLY UNRELIABLE!'
               write(*,*) '! Molecule', i, 'Charge: ', mol_core_chg(i)-mol_charge(i), ' electrons.'
					write(*,*) '! WARNING !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
            ENDIF
         ENDDO
         IF ( .NOT. charge_diff) THEN
            write(*,*) 'Neutral molecules'
         ENDIF
			DEALLOCATE(mol_charge)
         DEALLOCATE(mol_core_chg)
      ENDIF !myrank .EQ. 0

!---------------------------------------------------------------------------
!Start calculating the interactions:
      IF (myrank .EQ. 0) THEN
! Combine gridpoints to speed up the calculations:
         CALL combine_points(npts,chg,tot_points_now,ions%lattice(:,:),tot_combined,opts%fast_calc)
! Interactions between cores (Coulomb-, C6, and C12):
         CALL core_core(ions,coulombE,cellx,celly,cellz,nr_of_mol,nr_of_atoms,opts)
      ENDIF !myrank .EQ. 0

      CALL MPI_BCAST(ions%molecule(:),nr_of_atoms,MPI_INTEGER,0,MPI_COMM_WORLD,istatus,ierror)
      CALL MPI_BCAST(ions%new_car(:,:),3*nr_of_atoms,MPI_REAL8,0,MPI_COMM_WORLD,istatus,ierror)
      CALL MPI_BCAST(chg%e_charge(:,:,:),npts(1)*npts(2)*npts(3),MPI_REAL8,0,MPI_COMM_WORLD,istatus,ierror)
      CALL MPI_BCAST(chg%pos_car(:,:,:,:),3*npts(1)*npts(2)*npts(3),MPI_REAL8,0,MPI_COMM_WORLD,istatus,ierror)  
      CALL MPI_BCAST(chg%molecule(:,:,:),npts(1)*npts(2)*npts(3),MPI_INTEGER,0,MPI_COMM_WORLD,istatus,ierror)
      CALL MPI_BCAST(chg%atom(:,:,:),npts(1)*npts(2)*npts(3),MPI_INTEGER,0,MPI_COMM_WORLD,istatus,ierror)       
      CALL MPI_BCAST(tot_points_now,1,MPI_INTEGER,0,MPI_COMM_WORLD,istatus,ierror)

      ALLOCATE(chg_new%mol(tot_points_now))
      ALLOCATE(chg_new%e_charge(tot_points_now))
      ALLOCATE(chg_new%pos_car(tot_points_now,3))
      ALLOCATE(chg_new%atom(tot_points_now))
      chg_new%mol(:) = 0
      chg_new%e_charge(:) = 0
      chg_new%pos_car(:,:) = 0
      chg_new%atom(:) = 0

! Remove all points which are zero (after combine_points) to speed things up:   
      i = 0
      DO n3 = 1, npts(3)
         DO n2 = 1, npts(2)
            DO n1 = 1, npts(1)
               IF (chg%e_charge(n1,n2,n3) .NE. 0 ) THEN
                  i = i + 1
                  chg_new%mol(i) = chg%molecule(n1,n2,n3)
                  chg_new%e_charge(i) = chg%e_charge(n1,n2,n3)
                  chg_new%atom(i) = chg%atom(n1,n2,n3)
                  DO j = 1,3
                     chg_new%pos_car(i,j)  = chg%pos_car(n1,n2,n3,j)
                  ENDDO
               ENDIF
            ENDDO  
         ENDDO                
      ENDDO !n1

! Determine dipole of the unit cell	
      IF (myrank .EQ. 0) THEN
         CALL determine_dipole(chg_new,tot_points_now,ions)
      ENDIF

! Coulomb interaction between cores and gridpoints:
      CALL core_grid_mpi(chg,coulombE,cellx,celly,cellz,nr_of_mol,nr_inter_mol,myrank,nproc,npts,unitcells,nr_of_atoms,& 
        & ions%molecule(:),ions%new_car(:,:),ions%charge(:))  

      IF (.NOT. only_valence_e) THEN !removing this made valence only cube_files crash. Why? No clue....
         DEALLOCATE(chg%e_charge)
         DEALLOCATE(chg%molecule)
         DEALLOCATE(chg%atom)
         DEALLOCATE(chg%pos_car)
      ENDIF

! Coulomb-interaction between gridpoints:
      CALL grid_grid_mpi(chg_new,coulombE,cellx,celly,cellz,nr_of_mol,nr_inter_mol,myrank,nproc,unitcells,tot_points_now)

      DEALLOCATE(chg_new%mol)
      DEALLOCATE(chg_new%e_charge)
      DEALLOCATE(chg_new%pos_car)
      DEALLOCATE(chg_new%atom)

!! Write out the molecular interactions:
      IF (myrank .EQ. 0) THEN
         write(*,*) 'Molecular interactions calculated'
         ALLOCATE(coulombE%totalE(nr_inter_mol))
         ALLOCATE(coulombE%dist(nr_inter_mol))
         coulombE%totalE(:) = 0 
         coulombE%dist(:) = 0
         DO i = 1,nr_inter_mol ! determine distance between molecular centers and the total energy
            coulombE%totalE(i) = coulombE%c6(i) +coulombE%c12(i) + coulombE%grid_grid(i) +  & 
					&  coulombE%core_grid(i) + coulombE%core_core(i)
      		coulombE%dist(i) = sqrt( (mol_center(coulombE%mol1(i),1) - (mol_center(coulombE%mol2(i),1) + coulombE%distx(i)))**2 + & 
              & (mol_center(coulombE%mol1(i),2) - (mol_center(coulombE%mol2(i),2) + coulombE%disty(i)))**2 + & 
              & (mol_center(coulombE%mol1(i),3) - (mol_center(coulombE%mol2(i),3) + coulombE%distz(i)))**2 )
         ENDDO
         CALL write_interaction_files(coulombE,nr_inter_mol,nr_of_mol,mol_center,ions%lattice(:,:), & 
               &  opts%chargefile)
         DEALLOCATE(coulombE%totalE)
         DEALLOCATE(coulombE%dist)
      ENDIF         

      DEALLOCATE(ions%cov_rad)
      DEALLOCATE(ions%atomic_num)
      DEALLOCATE(ions%c6)
      DEALLOCATE(ions%vdw_dist)   
      DEALLOCATE(ions%molecule)
      DEALLOCATE(ions%new_car)
      DEALLOCATE(ions%charge)

   END SUBROUTINE molecules

END MODULE molecules_mod
