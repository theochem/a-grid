! Copyright 2009
! Wenjie Tang, Andri Arnaldsson, Samuel T. Chill, and Graeme Henkelman
!
! q-GRID and Bader are free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! A copy of the GNU General Public License is available at
! http://www.gnu.org/licenses/
!----------------------------------------------------------------------------------!
! q-GRID molecular interactions program, Version 1.0 (23/10/2015)
!
! Authors of the molecule interaction code: 
!	Niek de Klerk (Radboud University Nijmegen) <>
!	Joost van den Ende (Radboud University Nijmegen) <>
!
!	For more info see the article: 
!	q-GRID: a new method to calculate lattice and interaction energies
!  for molecular crystals from electron densities
!  Niek J.J. de Klerk, Joost A. van den Ende, Rita Bylsma, Peter Grancic, 
!  Gilles A. de Wijs, Herma M. Cuppen and Hugo Meekes
!  Crystal Growth & Design, 16, 2016, 662-671
!  dx.doi.org/10.1021/acs.cgd.5b01164
!
!-----------------------------------------------------------------------------------!
! The q-GRID code is build on top of: 
! Bader charge density analysis program
! Version 0.28a (07/12/12)
!
! Authors:
!   Wenjie Tang, Andri Arnaldsson, Samuel T. Chill, and Graeme Henkelman
!
! Authors of the multipole code:
!   Sebastien Lebegue <Sebastien.Lebegue@crm2.uhp-nancy.fr>
!   Angyan Janos <Janos.Angyan@crm2.uhp-nancy.fr>
!   Emmanuel Aubert <emmanuel.aubert@crm2.uhp-nancy.fr>
!
! Contributers:
!   Johannes Voss (DTU), Erik McNellis (FHI), Matthew Dyer (Liverpool),
!   Sören Wohlthat (Sydney)
!
! Based on algorithms described in the following publications:
!
!   A fast and robust algorithm for Bader decomposition of charge density
!   G. Henkelman, A. Arnaldsson, and H. Jonsson
!   Comput. Mater. Sci. 36, 254-360 (2006).
!
!   An improved grid-based algorithm for Bader charge allocation
!   E. Sanville, S. Kenny, R. Smith, and G. Henkelman
!   J. Comput. Chem. 28, 899-908 (2007).
!
!   A grid-based Bader analysis algorithm without lattice bias
!   W. Tang, E. Sanville, and G. Henkelman
!   J. Phys.: Condens. Matter 21, 084204 (2009)
!-----------------------------------------------------------------------------------!

     PROGRAM main_charge !MAIN_Charge

     USE mpi
     USE options_mod
     USE ions_mod
     USE charge_mod
     USE io_mod
     USE bader_mod 
     USE voronoi_mod
     USE chgcar_mod
     USE chgcar_mod
     USE multipole_mod
     USE molecules_mod    
     USE atoms_mod
     USE functions_mol_mod
     USE interactions_mod 

     IMPLICIT NONE

     ! Variables
     TYPE(options_obj) :: opts
     TYPE(ions_obj) :: ions
     TYPE(charge_obj) :: chgval
     TYPE(bader_obj) :: bdr
     TYPE(voronoi_obj) :: vor

     INTEGER :: ierror,nproc,myrank
     CHARACTER :: yes_or_no

! initialise MPI:
     CALL MPI_INIT(ierror)
     CALL MPI_COMM_SIZE(MPI_COMM_WORLD,nproc,ierror)
     CALL MPI_COMM_RANK(MPI_COMM_WORLD,myrank,ierror)

     ! Get the control variables
     CALL get_options(opts)

     IF (myrank .EQ. 0) THEN
   	 ! Write the version number
 	    WRITE(*,'(/,2X,A)') 'GRID BASED BADER ANALYSIS  (Version 0.28a 07/12/12) and & 
				&  molecular interactions calculations (Version 01/08/15)'

   	 ! Call the read routines from io_mod
   	 CALL read_charge(ions,chgval,opts)
     ENDIF !myrank == 0

!Bader analysis: all subroutines are in bader_mod.f90
     IF (opts%bader_flag) THEN
        IF (myrank .EQ. 0) THEN
     		  CALL bader_calc(bdr,ions,chgval,opts) 
     		  CALL bader_mindist(bdr,ions,chgval)   
     		  CALL bader_output(bdr,ions,chgval)    
     		  CALL multipole_calc(bdr,ions,chgval,opts)
        ENDIF !myrank == 0
     END IF
!End of Bader analysis

     IF (myrank .EQ. 0) THEN
    	 IF (opts%print_all_bader) CALL write_all_bader(bdr,opts,ions,chgval)
    	 IF (opts%print_all_atom) CALL write_all_atom(bdr,opts,ions,chgval)
       IF (opts%print_sel_atom) CALL write_sel_atom(bdr,opts,ions,chgval)
     	 IF (opts%print_sel_bader) CALL write_sel_bader(bdr,opts,ions,chgval)
       IF (opts%print_sum_atom) CALL write_sum_atom(bdr,opts,ions,chgval)
       IF (opts%print_sum_bader) CALL write_sum_bader(bdr,opts,ions,chgval)
       IF (opts%print_bader_index) CALL write_bader_index(bdr,opts,ions,chgval)
       IF (opts%print_atom_index) CALL write_atom_index(bdr,opts,ions,chgval)
       !Q
       IF (opts%refine_edge_itrs==-3) THEN
          PRINT*,'Print bader weights to CHGCAR files? y/n'     
          READ (*,*) yes_or_no
          IF (yes_or_no=='y') THEN
             CALL write_bader_weight(bdr,opts,ions,chgval)
          END IF
       END IF
       !Q
       IF (opts%dipole_flag) CALL multipole_calc(bdr,ions,chgval,opts)
       IF (opts%voronoi_flag) CALL voronoi(vor,ions,chgval)
       WRITE(*,*)
    ENDIF !myrank

!Determine vdWaals and coulomb interactions for molecules:            
    CALL molecules(bdr,ions,chgval,opts,nproc,myrank,ierror)
    CALL MPI_FINALIZE(ierror)
    IF (myrank .EQ. 0) THEN
       write(*,*) 'Program finished, all interactions calculated.'
       !Sometimes after this message an error appears, maybe this has something to do with the compiler or with MPI 
    ENDIF

    END PROGRAM main_charge

